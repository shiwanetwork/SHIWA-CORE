package org.example

import org.shiwanetwork.kernel.Ω
import org.shiwanetwork.security.hash.Hash

case class EmitSimpleSnapshot(lastSnapshotHash: Hash) extends Ω
