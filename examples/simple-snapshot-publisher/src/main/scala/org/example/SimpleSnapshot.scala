package org.example

import org.shiwanetwork.kernel.StateChannelSnapshot
import org.shiwanetwork.security.hash.Hash

case class SimpleSnapshot(lastSnapshotHash: Hash) extends StateChannelSnapshot
