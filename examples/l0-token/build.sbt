import Dependencies._

resolvers += Resolver.mavenLocal

ThisBuild / scalaVersion := "2.13.6"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "org.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "l0-token",
    libraryDependencies ++= List(
      Libraries.scalaTest,
      Libraries.catsCore,
      Libraries.catsEffect,
      Libraries.drosteCore,
      Libraries.drosteLaws,
      Libraries.drosteMacros,
      Libraries.shiwanetworkShared,
      Libraries.shiwanetworkKernel
    )
  )

/*Данный код на Scala является частью файла конфигурации проекта (build.sbt) и содержит настройки и зависимости для проекта, использующего инструмент сборки sbt.

1. `import Dependencies._` - данный код импортирует все зависимости, определенные в файле Dependencies.scala. Это позволяет использовать эти зависимости в проекте без явного указания их полного имени.

2. `resolvers += Resolver.mavenLocal` - данный код добавляет локальный репозиторий Maven в список репозиториев sbt. Это позволяет sbt искать и загружать зависимости из локального репозитория Maven.

3. `ThisBuild / scalaVersion := "2.13.6"` - данный код устанавливает версию Scala для проекта и указывает sbt использовать версию 2.13.6.

4. `ThisBuild / version := "0.1.0-SNAPSHOT"` - данный код устанавливает версию проекта и указывает sbt использовать версию "0.1.0-SNAPSHOT".

5. `ThisBuild / organization := "org.example"` - данный код устанавливает организацию проекта и указывает sbt использовать организацию "org.example".

6. `ThisBuild / organizationName := "example"` - данный код устанавливает имя организации проекта и указывает sbt использовать имя "example".

7. `lazy val root = (project in file("."))` - данный код создает основной проект в текущей директории.

8. `.settings()` - данный код указывает настройки проекта, которые будут применены к основному проекту.

9. `name := "l0-token"` - данный код устанавливает имя проекта и указывает sbt использовать имя "l0-token".

10. `libraryDependencies ++= List(...)` - данный код добавляет зависимости проекта в список библиотечных зависимостей. Каждая зависимость определена в файле Dependencies.scala и добавляется в список с помощью оператора `++=`.

В результате, код конфигурации указывает настройки проекта и добавляет необходимые зависимости для его сборки и исполнения.*/