# Определение атрибутов system и shell с возможностью указания значений по умолчанию.
{ system ? builtins.currentSystem or "unknown-system", shell ? "scala" }:

# Получение flake из текущего каталога с помощью указанной URL-схемы.
(builtins.getFlake ("git+file://" + toString ./.)).devShells.${system}.${shell}
# Выбор среды разработки (development shell) на основе system и shell.