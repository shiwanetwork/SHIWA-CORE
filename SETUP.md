
# Настройка

**Обновлено**: 01 сентября 2023 года

Руководство предназначено для настройки инструментов, которые вам понадобятся для разработки.

1. Установите _Java 11_, [ссылка](https://openjdk.java.net/projects/jdk/11/).
2. Установите _SBT_, [ссылка](https://www.scala-sbt.org/).

## Astra Linux
1. В разработке

## Fedora Linux

1. Установите [_Docker Engine_](https://docs.docker.com/engine/install/fedora/).
2. Выполните постустановочные шаги, [ссылка](https://docs.docker.com/engine/install/linux-postinstall/).
3. Установите [_kubectl_](https://docs.docker.com/desktop/kubernetes/).

Кроме того, просто установите [_Docker Desktop_](https://docs.docker.com/desktop/linux/install/fedora/) и активируйте [_Kubernetes_](https://docs.docker.com/desktop/kubernetes/).

## MacOS

1. Установите [_Docker Desktop_](https://docs.docker.com/desktop/mac/install/).
2. Активируйте _Kubernetes_ через _Docker Desktop_, [ссылка](https://docs.docker.com/desktop/kubernetes/).

# Установка Java 11

Установка _Java 11_ может различаться в зависимости от вашей операционной системы и выбранной методологии. Рекомендуется установить _OpenJDK 11_.

## Fedora Linux

Установка через _DNF_ является рекомендуемым методом для Fedora Linux. Инструкции можно найти [здесь](https://docs.fedoraproject.org/en-US/quick-docs/installing-java/).

## Mac OS

Установка через [_Homebrew_](https://brew.sh/) рекомендуется для Mac, но ее также можно использовать для других операционных систем. Чтобы установить _OpenJDK 11_, перейдите по [этой ссылке](https://formulae.brew.sh/formula/openjdk@11#default).

## SDKMan

[_SDKMan_](https://sdkman.io/) - это кроссплатформенный инструмент для управления версиями SDK. Инструкции по его использованию можно найти [здесь](https://sdkman.io/usage). Как и в других методах, рекомендуется установить _OpenJDK 11_. В случае, если это не является опцией, другие качественные реализации _JDK 11_ (например, _Coretto Java 11_, _Liberica Java 11_ или _Zulu Java 11_) подойдут.

# Установка SBT

Установите последнюю версию _SBT_, чтобы компилировать кодовую базу и запускать модульные тесты.

После установки _SBT_ его необходимо настроить. Инструкции можно найти в _CONTRIBUTING.md_.

## Fedora Linux

На Linux вы можете следовать инструкциям, предоставленным _SBT_, [ссылка](https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html).

## Mac OS

На Mac OS _SBT_ предоставляет инструкции [здесь](https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Mac.html). Его также можно установить через [_Homebrew_](https://formulae.brew.sh/formula/sbt#default). Также рекомендуется установить _SBTEnv_, [ссылка](https://formulae.brew.sh/formula/sbtenv#default). Это поможет настроить среду _SBT_.

# Запуск L0 и L1 на кластере EKS

## Предварительные требования

1. [sbt](https://www.scala-sbt.org/)
2. [Docker Desktop](https://www.docker.com/get-started/) с активированным [Kubernetes](https://docs.docker.com/desktop/kubernetes/)
3. [Skaffold CLI](https://skaffold.dev/docs/install/#standalone-binary)
4. [AWS CLI версии 2](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

## Настройка AWS
```
aws configure   # требует ключи доступа AWS
aws eks --region us-west-1 update-kubeconfig --name eks-dev
```

## Настройка кластера Kubernetes

### Обновите свой kubeconfig
```
kubectl config rename-context $(kubectl config current-context) eks-dev
```

### Создайте свое пространство имен
```
IAM_USER=$(aws sts get-caller-identity --query Arn --output text | sed 's/.*\///g')

kubectl create namespace $IAM_USER
kubectl config set-context --current --namespace=$IAM_USER
```

### Проверьте настройку Kubernetes
```
kubectl get pods
```

Должно вернуть:
```
No resources found in <your-namespace-name> namespace.
```

## Настройка репозитория образов Docker

### Установите утилиту управления учетными данными Docker
```
brew install docker-credential-helper-ecr
```

### Обновите конфигурацию Docker
Добавьте это в ваш `~/.docker/config.json` - если уже есть файл config.json, то добавьте только свойство credHelpers.
```json
{
  "credHelpers": {
    "public.ecr.aws": "ecr-login",
    "150340915792.dkr.ecr.us-west-1.amazonaws.com": "ecr-login"
  }
}
```

### Обновите конфигурацию Skaffold
```
skaffold config set default-repo 150340915792.dkr.ecr.us-west-1.amazonaws.com
```

### Проверьте настройку Docker
```
docker image ls 150340915792.dkr.ecr.us-west-1.amazonaws.com/l0-validator
```

Должны отобразиться существующие образы l0-validator.

## Создание образов и запуск кластера
```
skaffold dev --trigger manual --tail=false
```

Вы должны увидеть, что образы Docker успешно заг

ружены в реестр контейнеров, а затем ресурсы Kubernetes успешно развернуты на кластере EKS. Откройте Grafana для мониторинга производительности кластеров L0 и L1 по адресу [http://localhost:3000](http://localhost:3000).

Для доступа к API отдельных подов можно использовать HTTP-прокси. Сначала получите IP-адрес пода в кластере.
```
kubectl get pods -o wide
```

Затем установите переменную окружения `http_proxy` и используйте `curl` для запроса к поду.
```
export http_proxy=8080

curl <ip-адрес-пода>:9000/cluster/info
```

## Использование профилей

### Активация профилей
Активируйте профили, используя опцию `-p`. Профили также могут быть вручную деактивированы, добавив перед именем профиля префикс `-`.
```
skaffold dev -p foo,-bar
```

### Профили

- chaos - внедрение экспериментов с хаосом в кластер (например, отказ некоторых подов)