package org.shiwanetwork.rosetta.domain.api.construction

import cats.data.NonEmptyList

import org.shiwanetwork.ext.derevo.magnoliaCustomizable.snakeCaseConfiguration
import org.shiwanetwork.rosetta.domain._
import org.shiwanetwork.rosetta.domain.operation.Operation

import derevo.circe.magnolia.{customizableDecoder, customizableEncoder}
import derevo.derive

case object ConstructionPreprocess {
  @derive(customizableDecoder)
  case class Request(
    networkIdentifier: NetworkIdentifier,
    operations: List[Operation]
  )

  @derive(customizableEncoder)
  case class Response(
    requiredPublicKeys: Option[NonEmptyList[AccountIdentifier]]
  )
}
