package org.shiwanetwork.rosetta.domain.api.network

import org.shiwanetwork.ext.derevo.magnoliaCustomizable.snakeCaseConfiguration
import org.shiwanetwork.rosetta.domain.NetworkIdentifier

import derevo.circe.magnolia.customizableDecoder
import derevo.derive

object NetworkApiOptions {
  @derive(customizableDecoder)
  case class Request(networkIdentifier: NetworkIdentifier)
}
