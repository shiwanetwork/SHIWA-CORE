package org.shiwanetwork.rosetta.domain.api.construction

import org.shiwanetwork.ext.derevo.magnoliaCustomizable.snakeCaseConfiguration
import org.shiwanetwork.rosetta.domain.{NetworkIdentifier, TransactionIdentifier}
import org.shiwanetwork.security.hex.Hex

import derevo.circe.magnolia.{customizableDecoder, customizableEncoder}
import derevo.derive

case object ConstructionHash {
  @derive(customizableDecoder)
  case class Request(
    networkIdentifier: NetworkIdentifier,
    signedTransaction: Hex
  )

  @derive(customizableEncoder)
  case class Response(
    transactionIdentifier: TransactionIdentifier
  )
}
