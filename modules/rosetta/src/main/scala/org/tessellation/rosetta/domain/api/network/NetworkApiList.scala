package org.shiwanetwork.rosetta.domain.api.network

import org.shiwanetwork.ext.derevo.magnoliaCustomizable.snakeCaseConfiguration
import org.shiwanetwork.rosetta.domain.NetworkIdentifier

import derevo.circe.magnolia.customizableEncoder
import derevo.derive

object NetworkApiList {
  @derive(customizableEncoder)
  case class Response(networkIdentifiers: List[NetworkIdentifier])
}
