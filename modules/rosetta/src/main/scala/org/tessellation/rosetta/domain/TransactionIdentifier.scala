package org.shiwanetwork.rosetta.domain

import org.shiwanetwork.ext.derevo.magnoliaCustomizable.snakeCaseConfiguration
import org.shiwanetwork.security.hash.Hash

import derevo.cats.eqv
import derevo.circe.magnolia.customizableEncoder
import derevo.derive

@derive(eqv, customizableEncoder)
case class TransactionIdentifier(
  hash: Hash
)
