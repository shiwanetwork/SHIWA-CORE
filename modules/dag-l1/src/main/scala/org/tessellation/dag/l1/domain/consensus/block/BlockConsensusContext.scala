package org.shiwanetwork.dag.l1.domain.consensus.block

import java.security.KeyPair

import org.shiwanetwork.dag.l1.domain.block.BlockStorage
import org.shiwanetwork.dag.l1.domain.consensus.block.config.ConsensusConfig
import org.shiwanetwork.dag.l1.domain.consensus.block.http.p2p.clients.BlockConsensusClient
import org.shiwanetwork.dag.l1.domain.consensus.block.storage.ConsensusStorage
import org.shiwanetwork.dag.l1.domain.transaction.TransactionStorage
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.sdk.domain.block.processing.BlockValidator
import org.shiwanetwork.sdk.domain.cluster.storage.ClusterStorage
import org.shiwanetwork.sdk.domain.transaction.TransactionValidator

case class BlockConsensusContext[F[_]](
  blockConsensusClient: BlockConsensusClient[F],
  blockStorage: BlockStorage[F],
  blockValidator: BlockValidator[F],
  clusterStorage: ClusterStorage[F],
  consensusConfig: ConsensusConfig,
  consensusStorage: ConsensusStorage[F],
  keyPair: KeyPair,
  selfId: PeerId,
  transactionStorage: TransactionStorage[F],
  transactionValidator: TransactionValidator[F]
)
