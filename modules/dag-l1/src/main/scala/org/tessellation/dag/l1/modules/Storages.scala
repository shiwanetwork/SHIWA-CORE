package org.shiwanetwork.dag.l1.modules

import cats.effect.kernel.Async
import cats.effect.std.Random
import cats.syntax.flatMap._
import cats.syntax.functor._

import org.shiwanetwork.dag.l1.domain.address.storage.AddressStorage
import org.shiwanetwork.dag.l1.domain.block.BlockStorage
import org.shiwanetwork.dag.l1.domain.consensus.block.storage.ConsensusStorage
import org.shiwanetwork.dag.l1.domain.transaction.TransactionStorage
import org.shiwanetwork.dag.l1.infrastructure.address.storage.AddressStorage
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.peer.L0Peer
import org.shiwanetwork.schema.snapshot.{Snapshot, SnapshotInfo, StateProof}
import org.shiwanetwork.schema.transaction.TransactionReference
import org.shiwanetwork.sdk.domain.cluster.storage.{ClusterStorage, L0ClusterStorage, SessionStorage}
import org.shiwanetwork.sdk.domain.collateral.LatestBalances
import org.shiwanetwork.sdk.domain.node.NodeStorage
import org.shiwanetwork.sdk.domain.snapshot.storage.LastSnapshotStorage
import org.shiwanetwork.sdk.infrastructure.cluster.storage.L0ClusterStorage
import org.shiwanetwork.sdk.infrastructure.gossip.RumorStorage
import org.shiwanetwork.sdk.infrastructure.snapshot.storage.LastSnapshotStorage
import org.shiwanetwork.sdk.modules.SdkStorages

object Storages {

  def make[F[_]: Async: Random: KryoSerializer, P <: StateProof, S <: Snapshot, SI <: SnapshotInfo[P]](
    sdkStorages: SdkStorages[F],
    l0Peer: L0Peer
  ): F[Storages[F, P, S, SI]] =
    for {
      blockStorage <- BlockStorage.make[F]
      consensusStorage <- ConsensusStorage.make[F]
      l0ClusterStorage <- L0ClusterStorage.make[F](l0Peer)
      lastSnapshotStorage <- LastSnapshotStorage.make[F, S, SI]
      transactionStorage <- TransactionStorage.make[F](TransactionReference.empty)
      addressStorage <- AddressStorage.make[F]
    } yield
      new Storages[F, P, S, SI] {
        val address = addressStorage
        val block = blockStorage
        val consensus = consensusStorage
        val cluster = sdkStorages.cluster
        val l0Cluster = l0ClusterStorage
        val lastSnapshot = lastSnapshotStorage
        val node = sdkStorages.node
        val session = sdkStorages.session
        val rumor = sdkStorages.rumor
        val transaction = transactionStorage
      }
}

trait Storages[F[_], P <: StateProof, S <: Snapshot, SI <: SnapshotInfo[P]] {
  val address: AddressStorage[F]
  val block: BlockStorage[F]
  val consensus: ConsensusStorage[F]
  val cluster: ClusterStorage[F]
  val l0Cluster: L0ClusterStorage[F]
  val lastSnapshot: LastSnapshotStorage[F, S, SI] with LatestBalances[F]
  val node: NodeStorage[F]
  val session: SessionStorage[F]
  val rumor: RumorStorage[F]
  val transaction: TransactionStorage[F]
}
