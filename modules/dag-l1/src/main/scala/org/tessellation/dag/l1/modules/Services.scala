package org.shiwanetwork.dag.l1.modules

import cats.effect.kernel.Async

import org.shiwanetwork.dag.l1.config.types.AppConfig
import org.shiwanetwork.dag.l1.domain.block.BlockService
import org.shiwanetwork.dag.l1.domain.transaction.TransactionService
import org.shiwanetwork.dag.l1.http.p2p.P2PClient
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.snapshot.{Snapshot, SnapshotInfo, StateProof}
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshotInfo}
import org.shiwanetwork.sdk.domain.cluster.services.{Cluster, Session}
import org.shiwanetwork.sdk.domain.cluster.storage.L0ClusterStorage
import org.shiwanetwork.sdk.domain.collateral.Collateral
import org.shiwanetwork.sdk.domain.gossip.Gossip
import org.shiwanetwork.sdk.domain.healthcheck.LocalHealthcheck
import org.shiwanetwork.sdk.domain.snapshot.services.GlobalL0Service
import org.shiwanetwork.sdk.domain.snapshot.storage.LastSnapshotStorage
import org.shiwanetwork.sdk.infrastructure.Collateral
import org.shiwanetwork.sdk.infrastructure.block.processing.BlockAcceptanceManager
import org.shiwanetwork.sdk.modules.SdkServices
import org.shiwanetwork.security.SecurityProvider

object Services {

  def make[
    F[_]: Async: SecurityProvider: KryoSerializer,
    P <: StateProof,
    S <: Snapshot,
    SI <: SnapshotInfo[P]
  ](
    storages: Storages[F, P, S, SI],
    lastGlobalSnapshotStorage: LastSnapshotStorage[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo],
    globalL0Cluster: L0ClusterStorage[F],
    validators: Validators[F],
    sdkServices: SdkServices[F],
    p2PClient: P2PClient[F],
    cfg: AppConfig
  ): Services[F, P, S, SI] =
    new Services[F, P, S, SI] {
      val localHealthcheck = sdkServices.localHealthcheck
      val block = BlockService.make[F](
        BlockAcceptanceManager.make[F](validators.block),
        storages.address,
        storages.block,
        storages.transaction,
        cfg.collateral.amount
      )
      val cluster = sdkServices.cluster
      val gossip = sdkServices.gossip
      val globalL0 = GlobalL0Service
        .make[F](p2PClient.l0GlobalSnapshot, globalL0Cluster, lastGlobalSnapshotStorage, None)
      val session = sdkServices.session
      val transaction = TransactionService.make[F](storages.transaction, validators.transactionContextual)
      val collateral = Collateral.make[F](cfg.collateral, storages.lastSnapshot)
    }
}

trait Services[F[_], P <: StateProof, S <: Snapshot, SI <: SnapshotInfo[P]] {
  val localHealthcheck: LocalHealthcheck[F]
  val block: BlockService[F]
  val cluster: Cluster[F]
  val gossip: Gossip[F]
  val globalL0: GlobalL0Service[F]
  val session: Session[F]
  val transaction: TransactionService[F]
  val collateral: Collateral[F]
}
