package org.shiwanetwork.dag.l1.domain.consensus.block

import org.shiwanetwork.kernel.Ω
import org.shiwanetwork.schema.Block
import org.shiwanetwork.schema.round.RoundId
import org.shiwanetwork.security.Hashed

sealed trait BlockConsensusOutput extends Ω

object BlockConsensusOutput {
  case class FinalBlock(hashedBlock: Hashed[Block]) extends BlockConsensusOutput
  case class CleanedConsensuses(ids: Set[RoundId]) extends BlockConsensusOutput
  case object NoData extends BlockConsensusOutput
}
