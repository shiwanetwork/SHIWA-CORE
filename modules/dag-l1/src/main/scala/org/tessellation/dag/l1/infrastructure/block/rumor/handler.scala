package org.shiwanetwork.dag.l1.infrastructure.block.rumor

import cats.effect.Async
import cats.effect.std.Queue

import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.Block
import org.shiwanetwork.schema.gossip.CommonRumor
import org.shiwanetwork.sdk.infrastructure.gossip.RumorHandler
import org.shiwanetwork.security.signature.Signed

object handler {

  def blockRumorHandler[F[_]: Async: KryoSerializer](
    peerBlockQueue: Queue[F, Signed[Block]]
  ): RumorHandler[F] =
    RumorHandler.fromCommonRumorConsumer[F, Signed[Block]] {
      case CommonRumor(signedBlock) =>
        peerBlockQueue.offer(signedBlock)
    }
}
