package org.shiwanetwork.statechannel

import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.security.signature.Signed

import derevo.circe.magnolia.{decoder, encoder}
import derevo.derive

@derive(encoder, decoder)
case class StateChannelOutput(
  address: Address,
  snapshotBinary: Signed[StateChannelSnapshotBinary]
)
