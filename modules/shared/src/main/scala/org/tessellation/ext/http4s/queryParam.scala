package org.shiwanetwork.ext.http4s

import org.shiwanetwork.ext.derevo.Derive

import org.http4s.QueryParamDecoder

object queryParam extends Derive[QueryParamDecoder]
