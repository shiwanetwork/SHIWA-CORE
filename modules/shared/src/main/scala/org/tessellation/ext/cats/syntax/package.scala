package org.shiwanetwork.ext.cats

package object syntax {
  object next extends NextSyntax
  object partialPrevious extends PartialPreviousSyntax
  object validated extends ValidatedSyntax
}
