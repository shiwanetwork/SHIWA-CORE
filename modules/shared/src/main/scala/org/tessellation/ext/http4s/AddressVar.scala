package org.shiwanetwork.ext.http4s

import org.shiwanetwork.schema.address.{Address, DAGAddressRefined}

import eu.timepit.refined.refineV

object AddressVar {
  def unapply(str: String): Option[Address] = refineV[DAGAddressRefined](str).toOption.map(Address(_))
}
