package org.shiwanetwork.schema

import org.shiwanetwork.schema.height.{Height, SubHeight}
import org.shiwanetwork.schema.snapshot.Snapshot
import org.shiwanetwork.security.Hashed
import org.shiwanetwork.security.hash.{Hash, ProofsHash}

import derevo.cats.show
import derevo.derive

@derive(show)
case class SnapshotReference(
  height: Height,
  subHeight: SubHeight,
  ordinal: SnapshotOrdinal,
  lastSnapshotHash: Hash,
  hash: Hash,
  proofsHash: ProofsHash
)

object SnapshotReference {

  def fromHashedSnapshot(snapshot: Hashed[Snapshot]): SnapshotReference =
    SnapshotReference(
      snapshot.height,
      snapshot.subHeight,
      snapshot.ordinal,
      snapshot.lastSnapshotHash,
      snapshot.hash,
      snapshot.proofsHash
    )
}
