package org.shiwanetwork.schema

import cats.effect.Async
import cats.syntax.functor._

import org.shiwanetwork.ext.derevo.ordering
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.height.Height
import org.shiwanetwork.security.hash.ProofsHash
import org.shiwanetwork.security.signature.Signed

import derevo.cats.{order, show}
import derevo.circe.magnolia.{decoder, encoder}
import derevo.derive
import derevo.scalacheck.arbitrary

@derive(arbitrary, encoder, decoder, order, ordering, show)
case class BlockReference(height: Height, hash: ProofsHash)

object BlockReference {
  def of[F[_]: Async: KryoSerializer](block: Signed[Block]): F[BlockReference] =
    block.proofsHash.map(BlockReference(block.height, _))
}
