package org.shiwanetwork.currency.dataApplication

import org.shiwanetwork.schema.round.RoundId
import org.shiwanetwork.security.Hashed

import dataApplication.DataApplicationBlock

sealed trait ConsensusOutput

object ConsensusOutput {

  case class FinalBlock(hashedBlock: Hashed[DataApplicationBlock]) extends ConsensusOutput
  case class CleanedConsensuses(ids: Set[RoundId]) extends ConsensusOutput
  case object Noop extends ConsensusOutput
}
