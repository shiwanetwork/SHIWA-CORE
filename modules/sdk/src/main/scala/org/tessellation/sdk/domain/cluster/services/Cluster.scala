package org.shiwanetwork.sdk.domain.cluster.services

import org.shiwanetwork.schema.cluster.ClusterSessionToken
import org.shiwanetwork.schema.peer._
import org.shiwanetwork.security.signature.Signed

trait Cluster[F[_]] {
  def getRegistrationRequest: F[RegistrationRequest]
  def signRequest(signRequest: SignRequest): F[Signed[SignRequest]]
  def leave(): F[Unit]

  def info: F[Set[PeerInfo]]

  def createSession: F[ClusterSessionToken]
}
