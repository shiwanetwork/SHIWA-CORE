package org.shiwanetwork.sdk.domain.snapshot

import org.shiwanetwork.schema.peer.L0Peer

trait PeerSelect[F[_]] {
  def select: F[L0Peer]
}

object PeerSelect {
  val peerSelectLoggerName = "PeerSelectLogger"
}
