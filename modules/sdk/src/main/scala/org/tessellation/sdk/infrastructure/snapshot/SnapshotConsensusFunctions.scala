package org.shiwanetwork.sdk.infrastructure.snapshot

import cats.effect.Async
import cats.syntax.applicativeError._
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import cats.syntax.foldable._
import cats.syntax.functor._
import cats.syntax.option._
import cats.syntax.order._
import cats.{Applicative, Eq}

import scala.collection.immutable.{SortedMap, SortedSet}
import scala.util.control.NoStackTrace

import org.shiwanetwork.ext.cats.syntax.next._
import org.shiwanetwork.schema._
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.{Amount, Balance}
import org.shiwanetwork.schema.height.{Height, SubHeight}
import org.shiwanetwork.schema.snapshot.Snapshot
import org.shiwanetwork.sdk.domain.block.processing.{BlockAcceptanceResult, deprecationThreshold}
import org.shiwanetwork.sdk.domain.consensus.ConsensusFunctions
import org.shiwanetwork.sdk.domain.consensus.ConsensusFunctions.InvalidArtifact
import org.shiwanetwork.sdk.infrastructure.consensus.trigger.ConsensusTrigger
import org.shiwanetwork.security.SecurityProvider
import org.shiwanetwork.security.signature.Signed
import org.shiwanetwork.syntax.sortedCollection._

import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric.NonNegLong

case class InvalidHeight(lastHeight: Height, currentHeight: Height) extends NoStackTrace
case object NoTipsRemaining extends NoStackTrace
case object ArtifactMismatch extends InvalidArtifact

abstract class SnapshotConsensusFunctions[
  F[_]: Async: SecurityProvider,
  Event,
  Artifact <: Snapshot: Eq,
  Context,
  Trigger <: ConsensusTrigger
](implicit ordering: Ordering[BlockAsActiveTip])
    extends ConsensusFunctions[F, Event, SnapshotOrdinal, Artifact, Context] {

  def getRequiredCollateral: Amount

  def getBalances(context: Context): SortedMap[Address, Balance]

  def triggerPredicate(event: Event): Boolean = true

  def facilitatorFilter(lastSignedArtifact: Signed[Artifact], lastContext: Context, peerId: peer.PeerId): F[Boolean] =
    peerId.toAddress[F].map { address =>
      getBalances(lastContext).getOrElse(address, Balance.empty).satisfiesCollateral(getRequiredCollateral)
    }

  def validateArtifact(
    lastSignedArtifact: Signed[Artifact],
    lastContext: Context,
    trigger: ConsensusTrigger,
    artifact: Artifact
  ): F[Either[InvalidArtifact, (Artifact, Context)]]

  protected def getUpdatedTips(
    lastActive: SortedSet[ActiveTip],
    lastDeprecated: SortedSet[DeprecatedTip],
    acceptanceResult: BlockAcceptanceResult,
    currentOrdinal: SnapshotOrdinal
  ): (SortedSet[DeprecatedTip], SortedSet[ActiveTip], SortedSet[BlockAsActiveTip]) = {
    val usagesUpdate = acceptanceResult.contextUpdate.parentUsages
    val accepted =
      acceptanceResult.accepted.map { case (block, usages) => BlockAsActiveTip(block, usages) }.toSortedSet
    val (remainedActive, newlyDeprecated) = lastActive.partitionMap { at =>
      val maybeUpdatedUsage = usagesUpdate.get(at.block)
      Either.cond(
        maybeUpdatedUsage.exists(_ >= deprecationThreshold),
        DeprecatedTip(at.block, currentOrdinal),
        maybeUpdatedUsage.map(uc => at.copy(usageCount = uc)).getOrElse(at)
      )
    }.bimap(_.toSortedSet, _.toSortedSet)
    val lowestActiveIntroducedAt = remainedActive.toList.map(_.introducedAt).minimumOption.getOrElse(currentOrdinal)
    val remainedDeprecated = lastDeprecated.filter(_.deprecatedAt > lowestActiveIntroducedAt)

    (remainedDeprecated | newlyDeprecated, remainedActive, accepted)
  }

  protected def getTipsUsages(
    lastActive: Set[ActiveTip],
    lastDeprecated: Set[DeprecatedTip]
  ): Map[BlockReference, NonNegLong] = {
    val activeTipsUsages = lastActive.map(at => (at.block, at.usageCount)).toMap
    val deprecatedTipsUsages = lastDeprecated.map(dt => (dt.block, deprecationThreshold)).toMap

    activeTipsUsages ++ deprecatedTipsUsages
  }

  protected def getHeightAndSubHeight(
    lastGS: Artifact,
    deprecated: Set[DeprecatedTip],
    remainedActive: Set[ActiveTip],
    accepted: Set[BlockAsActiveTip]
  ): F[(Height, SubHeight)] = {
    val tipHeights = (deprecated.map(_.block.height) ++ remainedActive.map(_.block.height) ++ accepted
      .map(_.block.height)).toList

    for {
      height <- tipHeights.minimumOption.liftTo[F](NoTipsRemaining)

      _ <-
        if (height < lastGS.height)
          InvalidHeight(lastGS.height, height).raiseError
        else
          Applicative[F].unit

      subHeight = if (height === lastGS.height) lastGS.subHeight.next else SubHeight.MinValue
    } yield (height, subHeight)
  }

}
