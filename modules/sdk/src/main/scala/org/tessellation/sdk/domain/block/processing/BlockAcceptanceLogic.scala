package org.shiwanetwork.sdk.domain.block.processing

import cats.data.EitherT

import org.shiwanetwork.schema.Block
import org.shiwanetwork.sdk.domain.block.processing.{TxChains, UsageCount}
import org.shiwanetwork.security.signature.Signed

trait BlockAcceptanceLogic[F[_]] {
  def acceptBlock(
    block: Signed[Block],
    txChains: TxChains,
    context: BlockAcceptanceContext[F],
    contextUpdate: BlockAcceptanceContextUpdate
  ): EitherT[F, BlockNotAcceptedReason, (BlockAcceptanceContextUpdate, UsageCount)]

}
