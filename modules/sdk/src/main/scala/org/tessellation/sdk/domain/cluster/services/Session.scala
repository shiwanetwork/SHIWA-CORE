package org.shiwanetwork.sdk.domain.cluster.services

import org.shiwanetwork.schema.cluster.{SessionToken, TokenVerificationResult}
import org.shiwanetwork.schema.peer.PeerId

trait Session[F[_]] {
  def createSession: F[SessionToken]
  def verifyToken(peer: PeerId, headerToken: Option[SessionToken]): F[TokenVerificationResult]
}
