package org.shiwanetwork.sdk.infrastructure

import org.shiwanetwork.schema.gossip.Ordinal
import org.shiwanetwork.schema.peer.PeerId

package object consensus {
  type Bound = Map[PeerId, Ordinal]
}
