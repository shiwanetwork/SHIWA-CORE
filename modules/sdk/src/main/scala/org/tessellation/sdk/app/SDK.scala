package org.shiwanetwork.sdk.app

import java.security.KeyPair

import cats.effect.std.{Random, Supervisor}

import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.generation.Generation
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.schema.trust.PeerObservationAdjustmentUpdateBatch
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.http.p2p.SdkP2PClient
import org.shiwanetwork.sdk.infrastructure.metrics.Metrics
import org.shiwanetwork.sdk.modules._
import org.shiwanetwork.sdk.resources.SdkResources
import org.shiwanetwork.security.SecurityProvider

import fs2.concurrent.SignallingRef

trait SDK[F[_]] {
  implicit val random: Random[F]
  implicit val securityProvider: SecurityProvider[F]
  implicit val kryoPool: KryoSerializer[F]
  implicit val metrics: Metrics[F]
  implicit val supervisor: Supervisor[F]

  val keyPair: KeyPair
  lazy val nodeId: PeerId = PeerId.fromPublic(keyPair.getPublic)
  val generation: Generation
  val seedlist: Option[Set[SeedlistEntry]]
  val trustRatings: Option[PeerObservationAdjustmentUpdateBatch]

  val sdkResources: SdkResources[F]
  val sdkP2PClient: SdkP2PClient[F]
  val sdkQueues: SdkQueues[F]
  val sdkStorages: SdkStorages[F]
  val sdkServices: SdkServices[F]
  val sdkPrograms: SdkPrograms[F]
  val sdkValidators: SdkValidators[F]

  def restartSignal: SignallingRef[F, Unit]
}
