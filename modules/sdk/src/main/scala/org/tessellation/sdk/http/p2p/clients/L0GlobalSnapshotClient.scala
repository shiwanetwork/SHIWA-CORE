package org.shiwanetwork.sdk.http.p2p.clients

import cats.effect.Async

import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema._
import org.shiwanetwork.sdk.domain.cluster.services.Session
import org.shiwanetwork.sdk.http.p2p.PeerResponse
import org.shiwanetwork.sdk.http.p2p.PeerResponse.PeerResponse
import org.shiwanetwork.security.SecurityProvider
import org.shiwanetwork.security.signature.Signed

import org.http4s.Uri
import org.http4s.client.Client

trait L0GlobalSnapshotClient[F[_]] extends SnapshotClient[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo] {
  def getFull(ordinal: SnapshotOrdinal): PeerResponse[F, Signed[GlobalSnapshot]]
}

object L0GlobalSnapshotClient {
  def make[F[_]: Async: SecurityProvider: KryoSerializer](
    _client: Client[F],
    maybeSession: Option[Session[F]] = None
  ): L0GlobalSnapshotClient[F] =
    new L0GlobalSnapshotClient[F] {
      val client = _client
      val optionalSession = maybeSession
      val urlPrefix = "global-snapshots"

      def getFull(ordinal: SnapshotOrdinal): PeerResponse[F, Signed[GlobalSnapshot]] = {
        import org.shiwanetwork.ext.codecs.BinaryCodec.decoder

        PeerResponse[F, Signed[GlobalSnapshot]]((uri: Uri) => uri.addPath(s"$urlPrefix/${ordinal.value.value}").withQueryParam("full"))(
          client
        )
      }
    }
}
