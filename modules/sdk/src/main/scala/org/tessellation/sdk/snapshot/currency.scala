package org.shiwanetwork.sdk.snapshot

import org.shiwanetwork.currency.dataApplication.dataApplication.DataApplicationBlock
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.schema.Block
import org.shiwanetwork.sdk.infrastructure.snapshot.SnapshotConsensus
import org.shiwanetwork.security.signature.Signed

object currency {
  type CurrencySnapshotEvent = Either[Signed[Block], Signed[DataApplicationBlock]]

  type CurrencySnapshotArtifact = CurrencyIncrementalSnapshot

  type CurrencySnapshotConsensus[F[_]] =
    SnapshotConsensus[F, CurrencySnapshotArtifact, CurrencySnapshotContext, CurrencySnapshotEvent]
}
