package org.shiwanetwork.sdk

import org.shiwanetwork.schema.snapshot.{Snapshot, SnapshotInfo}
import org.shiwanetwork.schema.{Block, SnapshotOrdinal}
import org.shiwanetwork.sdk.infrastructure.consensus.Consensus
import org.shiwanetwork.security.signature.Signed

package object snapshot {

  type SnapshotEvent = Signed[Block]

  type SnapshotKey = SnapshotOrdinal

  type SnapshotArtifact[S <: Snapshot] = S

  type SnapshotContext[C <: SnapshotInfo[_]] = C

  type SnapshotConsensus[F[_], S <: Snapshot, C <: SnapshotInfo[_]] = Consensus[F, SnapshotEvent, SnapshotKey, SnapshotArtifact[S], C]

}
