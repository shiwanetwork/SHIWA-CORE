package org.shiwanetwork.sdk.infrastructure.snapshot.daemon

import cats.effect.Async
import cats.effect.std.Supervisor
import cats.syntax.eq._
import cats.syntax.functor._

import org.shiwanetwork.schema.node.NodeState
import org.shiwanetwork.sdk.domain.Daemon
import org.shiwanetwork.sdk.domain.node.NodeStorage
import org.shiwanetwork.sdk.domain.snapshot.programs.Download

trait DownloadDaemon[F[_]] extends Daemon[F] {}

object DownloadDaemon {

  def make[F[_]: Async](nodeStorage: NodeStorage[F], download: Download[F])(implicit S: Supervisor[F]) = new DownloadDaemon[F] {
    def start: F[Unit] = S.supervise(watchForDownload).void

    private def watchForDownload: F[Unit] =
      nodeStorage.nodeStates
        .filter(_ === NodeState.WaitingForDownload)
        .evalTap { _ =>
          download.download
        }
        .compile
        .drain
  }
}
