package org.shiwanetwork.sdk.modules

import cats.data.NonEmptySet
import cats.effect.Async

import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.sdk.domain.block.processing.BlockValidator
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.domain.statechannel.StateChannelValidator
import org.shiwanetwork.sdk.domain.transaction.{TransactionChainValidator, TransactionValidator}
import org.shiwanetwork.sdk.infrastructure.block.processing.BlockValidator
import org.shiwanetwork.sdk.infrastructure.gossip.RumorValidator
import org.shiwanetwork.security.SecurityProvider
import org.shiwanetwork.security.signature.SignedValidator

object SdkValidators {

  def make[F[_]: Async: KryoSerializer: SecurityProvider](
    l0Seedlist: Option[Set[SeedlistEntry]],
    seedlist: Option[Set[SeedlistEntry]],
    stateChannelAllowanceLists: Option[Map[Address, NonEmptySet[PeerId]]]
  ): SdkValidators[F] = {
    val signedValidator = SignedValidator.make[F]
    val transactionChainValidator = TransactionChainValidator.make[F]
    val transactionValidator = TransactionValidator.make[F](signedValidator)
    val blockValidator = BlockValidator.make[F](signedValidator, transactionChainValidator, transactionValidator)
    val currencyTransactionChainValidator = TransactionChainValidator.make[F]
    val currencyTransactionValidator = TransactionValidator.make[F](signedValidator)
    val currencyBlockValidator = BlockValidator
      .make[F](signedValidator, currencyTransactionChainValidator, currencyTransactionValidator)
    val rumorValidator = RumorValidator.make[F](seedlist, signedValidator)
    val stateChannelValidator = StateChannelValidator.make[F](signedValidator, l0Seedlist, stateChannelAllowanceLists)

    new SdkValidators[F](
      signedValidator,
      transactionChainValidator,
      transactionValidator,
      currencyTransactionChainValidator,
      currencyTransactionValidator,
      blockValidator,
      currencyBlockValidator,
      rumorValidator,
      stateChannelValidator
    ) {}
  }
}

sealed abstract class SdkValidators[F[_]] private (
  val signedValidator: SignedValidator[F],
  val transactionChainValidator: TransactionChainValidator[F],
  val transactionValidator: TransactionValidator[F],
  val currencyTransactionChainValidator: TransactionChainValidator[F],
  val currencyTransactionValidator: TransactionValidator[F],
  val blockValidator: BlockValidator[F],
  val currencyBlockValidator: BlockValidator[F],
  val rumorValidator: RumorValidator[F],
  val stateChannelValidator: StateChannelValidator[F]
)
