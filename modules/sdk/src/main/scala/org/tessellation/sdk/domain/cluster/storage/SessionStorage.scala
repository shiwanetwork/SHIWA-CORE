package org.shiwanetwork.sdk.domain.cluster.storage

import org.shiwanetwork.schema.cluster.SessionToken

trait SessionStorage[F[_]] {
  def createToken: F[SessionToken]
  def getToken: F[Option[SessionToken]]
  def clearToken: F[Unit]
}
