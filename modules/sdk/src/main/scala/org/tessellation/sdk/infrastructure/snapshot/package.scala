package org.shiwanetwork.sdk.infrastructure

import org.shiwanetwork.schema.SnapshotOrdinal
import org.shiwanetwork.schema.snapshot.Snapshot
import org.shiwanetwork.sdk.infrastructure.consensus.Consensus

package object snapshot {

  type SnapshotArtifact[S <: Snapshot] = S

  type SnapshotConsensus[F[_], S <: Snapshot, Context, Event] =
    Consensus[F, Event, SnapshotOrdinal, SnapshotArtifact[S], Context]

}
