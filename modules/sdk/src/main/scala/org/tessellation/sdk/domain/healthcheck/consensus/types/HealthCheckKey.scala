package org.shiwanetwork.sdk.domain.healthcheck.consensus.types

import org.shiwanetwork.schema.peer.PeerId

trait HealthCheckKey {
  def id: PeerId
}
