package org.shiwanetwork.sdk.domain.block.processing

import org.shiwanetwork.schema.Block
import org.shiwanetwork.security.signature.Signed

import derevo.cats.show
import derevo.derive
import eu.timepit.refined.cats._
import eu.timepit.refined.types.numeric.NonNegLong

@derive(show)
case class BlockAcceptanceResult(
  contextUpdate: BlockAcceptanceContextUpdate,
  accepted: List[(Signed[Block], NonNegLong)],
  notAccepted: List[(Signed[Block], BlockNotAcceptedReason)]
)
