package org.shiwanetwork.sdk.domain.block.processing

import org.shiwanetwork.schema.Block
import org.shiwanetwork.sdk.domain.block.processing.UsageCount
import org.shiwanetwork.security.signature.Signed

trait BlockAcceptanceManager[F[_]] {

  def acceptBlocksIteratively(
    blocks: List[Signed[Block]],
    context: BlockAcceptanceContext[F]
  ): F[BlockAcceptanceResult]

  def acceptBlock(
    block: Signed[Block],
    context: BlockAcceptanceContext[F]
  ): F[Either[BlockNotAcceptedReason, (BlockAcceptanceContextUpdate, UsageCount)]]

}
