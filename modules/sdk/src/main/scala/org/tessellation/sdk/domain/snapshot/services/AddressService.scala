package org.shiwanetwork.sdk.domain.snapshot.services

import org.shiwanetwork.schema.SnapshotOrdinal
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.Balance
import org.shiwanetwork.schema.snapshot.Snapshot

trait AddressService[F[_], S <: Snapshot] {
  def getBalance(address: Address): F[Option[(Balance, SnapshotOrdinal)]]
  def getBalance(ordinal: SnapshotOrdinal, address: Address): F[Option[(Balance, SnapshotOrdinal)]]
  def getTotalSupply: F[Option[(BigInt, SnapshotOrdinal)]]
  def getFilteredOutTotalSupply: F[Option[(BigInt, SnapshotOrdinal)]]

  def getTotalSupply(ordinal: SnapshotOrdinal): F[Option[(BigInt, SnapshotOrdinal)]]
  def getWalletCount: F[Option[(Int, SnapshotOrdinal)]]
  def getWalletCount(ordinal: SnapshotOrdinal): F[Option[(Int, SnapshotOrdinal)]]
}
