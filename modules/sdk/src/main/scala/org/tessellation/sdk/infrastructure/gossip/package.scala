package org.shiwanetwork.sdk.infrastructure

import cats.data.{Kleisli, OptionT}

import org.shiwanetwork.schema.gossip.RumorRaw
import org.shiwanetwork.schema.peer.PeerId

package object gossip {

  type RumorHandler[F[_]] = Kleisli[OptionT[F, *], (RumorRaw, PeerId), Unit]

  val rumorLoggerName = "RumorLogger"

}
