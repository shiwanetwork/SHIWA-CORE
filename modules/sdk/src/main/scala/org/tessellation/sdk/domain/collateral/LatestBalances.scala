package org.shiwanetwork.sdk.domain.collateral

import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.Balance

import fs2.Stream

trait LatestBalances[F[_]] {
  def getLatestBalances: F[Option[Map[Address, Balance]]]
  def getLatestBalancesStream: Stream[F, Map[Address, Balance]]
}
