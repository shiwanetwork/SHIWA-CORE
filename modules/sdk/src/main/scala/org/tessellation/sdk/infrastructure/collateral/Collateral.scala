package org.shiwanetwork.sdk.infrastructure

import cats.effect.Async
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.order._

import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.{Amount, Balance}
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.sdk.config.types.CollateralConfig
import org.shiwanetwork.sdk.domain.collateral.{Collateral, LatestBalances}
import org.shiwanetwork.security.SecurityProvider
import org.shiwanetwork.security.key.ops.PublicKeyOps

import eu.timepit.refined.auto._

object Collateral {

  def make[F[_]: Async: SecurityProvider](
    config: CollateralConfig,
    latestBalances: LatestBalances[F]
  ): Collateral[F] =
    new Collateral[F] {

      def hasCollateral(peerId: PeerId): F[Boolean] =
        peerId.value.toPublicKey
          .map(_.toAddress)
          .flatMap(getBalance)
          .map(_.map(_ >= config.amount).getOrElse(true))

      private def getBalance(address: Address): F[Option[Amount]] =
        latestBalances.getLatestBalances
          .map(_.map(_.withDefaultValue(Balance.empty)(address)))
    }
}
