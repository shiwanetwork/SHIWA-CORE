package org.shiwanetwork.sdk.domain.rewards

import scala.collection.immutable.{SortedMap, SortedSet}

import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.Balance
import org.shiwanetwork.schema.snapshot.{IncrementalSnapshot, StateProof}
import org.shiwanetwork.schema.transaction.{RewardTransaction, Transaction}
import org.shiwanetwork.sdk.infrastructure.consensus.trigger.ConsensusTrigger
import org.shiwanetwork.security.signature.Signed

trait Rewards[F[_], P <: StateProof, S <: IncrementalSnapshot[P]] {
  def distribute(
    lastArtifact: Signed[S],
    lastBalances: SortedMap[Address, Balance],
    acceptedTransactions: SortedSet[Signed[Transaction]],
    trigger: ConsensusTrigger
  ): F[SortedSet[RewardTransaction]]
}
