package org.shiwanetwork.sdk.domain

import scala.util.control.NoStackTrace

package object collateral {
  case object OwnCollateralNotSatisfied extends NoStackTrace
}
