package org.shiwanetwork.sdk.domain.snapshot.storage

import org.shiwanetwork.schema.SnapshotOrdinal
import org.shiwanetwork.schema.height.Height
import org.shiwanetwork.schema.snapshot.{Snapshot, SnapshotInfo}
import org.shiwanetwork.security.Hashed

trait LastSnapshotStorage[F[_], S <: Snapshot, SI <: SnapshotInfo[_]] {
  def set(snapshot: Hashed[S], state: SI): F[Unit]
  def setInitial(snapshot: Hashed[S], state: SI): F[Unit]
  def get: F[Option[Hashed[S]]]
  def getCombined: F[Option[(Hashed[S], SI)]]
  def getOrdinal: F[Option[SnapshotOrdinal]]
  def getHeight: F[Option[Height]]
}
