package org.shiwanetwork.sdk.infrastructure.healthcheck.ping

import org.shiwanetwork.schema._
import org.shiwanetwork.schema.cluster.SessionToken
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.sdk.domain.healthcheck.consensus.types.HealthCheckKey

import com.comcast.ip4s.{Host, Port}
import derevo.cats.show
import derevo.circe.magnolia.{decoder, encoder}
import derevo.derive

@derive(encoder, decoder, show)
case class PingHealthCheckKey(id: PeerId, ip: Host, p2pPort: Port, session: SessionToken) extends HealthCheckKey
