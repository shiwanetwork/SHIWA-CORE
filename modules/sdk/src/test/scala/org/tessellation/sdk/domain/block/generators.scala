package org.shiwanetwork.sdk.domain.block

import cats.data.{NonEmptyList, NonEmptySet}

import scala.collection.immutable.SortedSet

import org.shiwanetwork.schema.generators.{signedOf, signedTransactionGen}
import org.shiwanetwork.schema.{Block, BlockReference}
import org.shiwanetwork.security.signature.Signed

import org.scalacheck.{Arbitrary, Gen}

object generators {

  val blockReferencesGen: Gen[NonEmptyList[BlockReference]] =
    Gen.nonEmptyListOf(Arbitrary.arbitrary[BlockReference]).map(NonEmptyList.fromListUnsafe(_))

  val blockGen: Gen[Block] =
    for {
      blockReferences <- blockReferencesGen
      signedTxn <- signedTransactionGen
    } yield Block(blockReferences, NonEmptySet.fromSetUnsafe(SortedSet(signedTxn)))

  val signedBlockGen: Gen[Signed[Block]] = signedOf(blockGen)
  implicit val signedBlockArbitrary = Arbitrary(signedBlockGen)

}
