package org.shiwanetwork.currency.l0.cell

import org.shiwanetwork.currency.l0.snapshot.CurrencySnapshotEvent
import org.shiwanetwork.kernel.Ω

sealed trait AlgebraCommand extends Ω

object AlgebraCommand {
  case class EnqueueL1BlockData(data: CurrencySnapshotEvent) extends AlgebraCommand
  case object NoAction extends AlgebraCommand
}
