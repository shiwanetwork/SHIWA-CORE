package org.shiwanetwork.currency.l0.snapshot

import cats.effect.Async
import cats.syntax.option._

import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.sdk.domain.cluster.services.Session
import org.shiwanetwork.sdk.http.p2p.clients.SnapshotClient
import org.shiwanetwork.security.SecurityProvider

import org.http4s.client.Client

object CurrencySnapshotClient {
  type CurrencySnapshotClient[F[_]] = SnapshotClient[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo]

  def make[F[_]: Async: SecurityProvider: KryoSerializer](_client: Client[F], session: Session[F]): CurrencySnapshotClient[F] =
    new SnapshotClient[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo] {
      val client = _client
      val optionalSession = session.some
      val urlPrefix = "snapshots"
    }
}
