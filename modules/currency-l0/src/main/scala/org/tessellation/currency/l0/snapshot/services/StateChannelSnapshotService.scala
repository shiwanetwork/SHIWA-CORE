package org.shiwanetwork.currency.l0.snapshot.services

import java.security.KeyPair

import cats.Applicative
import cats.data.NonEmptyList
import cats.effect.Async
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.show._

import org.shiwanetwork.currency.l0.node.IdentifierStorage
import org.shiwanetwork.currency.l0.snapshot.CurrencySnapshotArtifact
import org.shiwanetwork.currency.l0.snapshot.storages.LastBinaryHashStorage
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.ext.crypto._
import org.shiwanetwork.json.JsonBinarySerializer
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.sdk.domain.cluster.storage.L0ClusterStorage
import org.shiwanetwork.sdk.domain.snapshot.storage.SnapshotStorage
import org.shiwanetwork.sdk.domain.statechannel.StateChannelValidator.StateChannelValidationError
import org.shiwanetwork.sdk.http.p2p.clients.StateChannelSnapshotClient
import org.shiwanetwork.security.hash.Hash
import org.shiwanetwork.security.signature.Signed
import org.shiwanetwork.security.{Hashed, SecurityProvider}
import org.shiwanetwork.statechannel.StateChannelSnapshotBinary

import org.typelevel.log4cats.slf4j.Slf4jLogger
import retry._

trait StateChannelSnapshotService[F[_]] {

  def consume(signedArtifact: Signed[CurrencySnapshotArtifact], context: CurrencySnapshotContext): F[Unit]
  def createGenesisBinary(snapshot: Signed[CurrencySnapshot]): F[Signed[StateChannelSnapshotBinary]]
  def createBinary(snapshot: Signed[CurrencySnapshotArtifact]): F[Signed[StateChannelSnapshotBinary]]
}

object StateChannelSnapshotService {
  def make[F[_]: Async: KryoSerializer: SecurityProvider](
    keyPair: KeyPair,
    lastBinaryHashStorage: LastBinaryHashStorage[F],
    stateChannelSnapshotClient: StateChannelSnapshotClient[F],
    globalL0ClusterStorage: L0ClusterStorage[F],
    snapshotStorage: SnapshotStorage[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo],
    identifierStorage: IdentifierStorage[F]
  ): StateChannelSnapshotService[F] =
    new StateChannelSnapshotService[F] {
      private val logger = Slf4jLogger.getLogger

      private val sendRetries = 5

      private val retryPolicy: RetryPolicy[F] = RetryPolicies.limitRetries(sendRetries)

      private def wasSuccessful: Either[NonEmptyList[StateChannelValidationError], Unit] => F[Boolean] =
        _.isRight.pure[F]

      private def onFailure(binaryHashed: Hashed[StateChannelSnapshotBinary]) =
        (_: Either[NonEmptyList[StateChannelValidationError], Unit], details: RetryDetails) =>
          logger.info(s"Retrying sending ${binaryHashed.hash.show} to Global L0 after rejection. Retries so far ${details.retriesSoFar}")

      private def onError(binaryHashed: Hashed[StateChannelSnapshotBinary]) = (_: Throwable, details: RetryDetails) =>
        logger.info(s"Retrying sending ${binaryHashed.hash.show} to Global L0 after error. Retries so far ${details.retriesSoFar}")

      def createGenesisBinary(snapshot: Signed[CurrencySnapshot]): F[Signed[StateChannelSnapshotBinary]] = {
        val bytes = JsonBinarySerializer.serialize(snapshot)
        StateChannelSnapshotBinary(Hash.empty, bytes, SnapshotFee.MinValue).sign(keyPair)
      }

      def createBinary(snapshot: Signed[CurrencySnapshotArtifact]): F[Signed[StateChannelSnapshotBinary]] = for {
        lastSnapshotBinaryHash <- lastBinaryHashStorage.get
        bytes = JsonBinarySerializer.serialize(snapshot)
        binary <- StateChannelSnapshotBinary(lastSnapshotBinaryHash, bytes, SnapshotFee.MinValue).sign(keyPair)
      } yield binary

      def consume(signedArtifact: Signed[CurrencySnapshotArtifact], context: CurrencySnapshotContext): F[Unit] = for {
        binary <- createBinary(signedArtifact)
        binaryHashed <- binary.toHashed
        identifier <- identifierStorage.get
        _ <- retryingOnFailuresAndAllErrors[Either[NonEmptyList[StateChannelValidationError], Unit]](
          retryPolicy,
          wasSuccessful,
          onFailure(binaryHashed),
          onError(binaryHashed)
        )(
          globalL0ClusterStorage.getRandomPeer.flatMap { l0Peer =>
            stateChannelSnapshotClient
              .send(identifier, binary)(l0Peer)
              .onError(e => logger.warn(e)(s"Sending ${binaryHashed.hash.show} snapshot to Global L0 peer ${l0Peer.show} failed!"))
              .flatTap {
                case Right(_) => logger.info(s"Sent ${binaryHashed.hash.show} to Global L0 peer ${l0Peer.show}")
                case Left(errors) =>
                  logger.error(s"Snapshot ${binaryHashed.hash.show} rejected by Global L0 peer ${l0Peer.show}. Reasons: ${errors.show}")
              }
          }
        )
        _ <- lastBinaryHashStorage.set(binaryHashed.hash)
        _ <- snapshotStorage
          .prepend(signedArtifact, context.snapshotInfo)
          .ifM(
            Applicative[F].unit,
            logger.error("Cannot save CurrencySnapshot into the storage")
          )
      } yield ()
    }
}
