package org.shiwanetwork.currency.l0.http.p2p

import cats.effect.Async

import org.shiwanetwork.currency.l0.snapshot.CurrencySnapshotClient
import org.shiwanetwork.currency.l0.snapshot.CurrencySnapshotClient.CurrencySnapshotClient
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.sdk.domain.cluster.services.Session
import org.shiwanetwork.sdk.http.p2p.SdkP2PClient
import org.shiwanetwork.sdk.http.p2p.clients._
import org.shiwanetwork.sdk.infrastructure.gossip.p2p.GossipClient
import org.shiwanetwork.security.SecurityProvider

import org.http4s.client.Client

object P2PClient {

  def make[F[_]: Async: SecurityProvider: KryoSerializer](
    sdkP2PClient: SdkP2PClient[F],
    client: Client[F],
    session: Session[F]
  ): P2PClient[F] =
    new P2PClient[F](
      L0ClusterClient.make(client),
      sdkP2PClient.cluster,
      sdkP2PClient.gossip,
      sdkP2PClient.node,
      StateChannelSnapshotClient.make(client),
      L0GlobalSnapshotClient.make(client),
      CurrencySnapshotClient.make[F](client, session),
      L0TrustClient.make(client)
    ) {}
}

sealed abstract class P2PClient[F[_]] private (
  val globalL0Cluster: L0ClusterClient[F],
  val cluster: ClusterClient[F],
  val gossip: GossipClient[F],
  val node: NodeClient[F],
  val stateChannelSnapshot: StateChannelSnapshotClient[F],
  val l0GlobalSnapshot: L0GlobalSnapshotClient[F],
  val currencySnapshot: CurrencySnapshotClient[F],
  val l0Trust: L0TrustClient[F]
)
