package org.shiwanetwork.currency.l0.config

import org.shiwanetwork.cli.AppEnvironment
import org.shiwanetwork.schema.peer.L0Peer
import org.shiwanetwork.sdk.config.types._

object types {
  case class AppConfig(
    environment: AppEnvironment,
    http: HttpConfig,
    gossip: GossipConfig,
    healthCheck: HealthCheckConfig,
    snapshot: SnapshotConfig,
    collateral: CollateralConfig,
    globalL0Peer: L0Peer
  )
}
