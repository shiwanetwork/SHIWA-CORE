package org.shiwanetwork.currency.l0.modules

import java.security.KeyPair

import cats.effect.Async
import cats.effect.std.Random

import org.shiwanetwork.currency.l0.http.p2p.P2PClient
import org.shiwanetwork.currency.l0.snapshot.programs.{Download, Genesis, Rollback}
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.peer.{L0Peer, PeerId}
import org.shiwanetwork.sdk.domain.cluster.programs.{Joining, L0PeerDiscovery, PeerDiscovery}
import org.shiwanetwork.sdk.domain.snapshot.PeerSelect
import org.shiwanetwork.sdk.domain.snapshot.programs.Download
import org.shiwanetwork.sdk.infrastructure.genesis.{Loader => GenesisLoader}
import org.shiwanetwork.sdk.infrastructure.snapshot.{CurrencySnapshotContextFunctions, MajorityPeerSelect}
import org.shiwanetwork.sdk.modules.SdkPrograms
import org.shiwanetwork.security.SecurityProvider

object Programs {

  def make[F[_]: Async: Random: KryoSerializer: SecurityProvider](
    keyPair: KeyPair,
    nodeId: PeerId,
    globalL0Peer: L0Peer,
    sdkPrograms: SdkPrograms[F],
    storages: Storages[F],
    services: Services[F],
    p2pClient: P2PClient[F],
    currencySnapshotContextFns: CurrencySnapshotContextFunctions[F]
  ): Programs[F] = {
    val peerSelect: PeerSelect[F] = MajorityPeerSelect.make(storages.cluster, p2pClient.currencySnapshot)
    val download = Download
      .make(
        p2pClient,
        storages.cluster,
        currencySnapshotContextFns,
        storages.node,
        services.consensus,
        peerSelect,
        storages.identifier
      )

    val globalL0PeerDiscovery = L0PeerDiscovery.make(
      p2pClient.globalL0Cluster,
      storages.globalL0Cluster
    )

    val genesisLoader = GenesisLoader.make

    val genesis = Genesis.make(
      keyPair,
      services.collateral,
      storages.lastBinaryHash,
      services.stateChannelSnapshot,
      storages.snapshot,
      p2pClient.stateChannelSnapshot,
      globalL0Peer,
      nodeId,
      services.consensus.manager,
      genesisLoader,
      storages.identifier
    )

    val rollback = Rollback.make(
      nodeId,
      services.globalL0,
      storages.identifier,
      storages.lastBinaryHash,
      storages.snapshot,
      services.collateral,
      services.consensus.manager
    )

    new Programs[F](sdkPrograms.peerDiscovery, globalL0PeerDiscovery, sdkPrograms.joining, download, genesis, rollback) {}
  }
}

sealed abstract class Programs[F[_]] private (
  val peerDiscovery: PeerDiscovery[F],
  val globalL0PeerDiscovery: L0PeerDiscovery[F],
  val joining: Joining[F],
  val download: Download[F],
  val genesis: Genesis[F],
  val rollback: Rollback[F]
)
