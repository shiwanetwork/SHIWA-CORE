package org.shiwanetwork.currency.l0

import org.shiwanetwork.currency.dataApplication.dataApplication.DataApplicationBlock
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.schema.Block
import org.shiwanetwork.sdk.infrastructure.snapshot._
import org.shiwanetwork.security.signature.Signed

package object snapshot {

  type CurrencySnapshotEvent = Either[Signed[Block], Signed[DataApplicationBlock]]

  type CurrencySnapshotArtifact = CurrencyIncrementalSnapshot

  type CurrencySnapshotConsensus[F[_]] =
    SnapshotConsensus[F, CurrencySnapshotArtifact, CurrencySnapshotContext, CurrencySnapshotEvent]

}
