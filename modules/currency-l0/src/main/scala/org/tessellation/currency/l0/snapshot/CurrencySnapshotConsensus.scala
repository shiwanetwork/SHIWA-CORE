package org.shiwanetwork.currency.l0.snapshot

import java.security.KeyPair

import cats.effect.kernel.Async
import cats.effect.std.{Random, Supervisor}

import org.shiwanetwork.currency.dataApplication.{BaseDataApplicationL0Service, DataUpdate, L0NodeContext}
import org.shiwanetwork.currency.l0.snapshot.services.StateChannelSnapshotService
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.SnapshotOrdinal
import org.shiwanetwork.schema.balance.Amount
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.sdk.config.types.SnapshotConfig
import org.shiwanetwork.sdk.domain.cluster.services.Session
import org.shiwanetwork.sdk.domain.cluster.storage.ClusterStorage
import org.shiwanetwork.sdk.domain.gossip.Gossip
import org.shiwanetwork.sdk.domain.node.NodeStorage
import org.shiwanetwork.sdk.domain.rewards.Rewards
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.infrastructure.consensus.Consensus
import org.shiwanetwork.sdk.infrastructure.metrics.Metrics
import org.shiwanetwork.sdk.infrastructure.snapshot.{CurrencySnapshotCreator, CurrencySnapshotValidator, SnapshotConsensus}
import org.shiwanetwork.security.SecurityProvider

import io.circe.Decoder
import io.circe.disjunctionCodecs._
import org.http4s.client.Client

object CurrencySnapshotConsensus {

  def make[F[_]: Async: Random: KryoSerializer: SecurityProvider: Metrics: Supervisor: L0NodeContext](
    gossip: Gossip[F],
    selfId: PeerId,
    keyPair: KeyPair,
    seedlist: Option[Set[SeedlistEntry]],
    collateral: Amount,
    clusterStorage: ClusterStorage[F],
    nodeStorage: NodeStorage[F],
    maybeRewards: Option[Rewards[F, CurrencySnapshotStateProof, CurrencyIncrementalSnapshot]],
    snapshotConfig: SnapshotConfig,
    client: Client[F],
    session: Session[F],
    stateChannelSnapshotService: StateChannelSnapshotService[F],
    maybeDataApplication: Option[BaseDataApplicationL0Service[F]],
    creator: CurrencySnapshotCreator[F],
    validator: CurrencySnapshotValidator[F]
  ): F[
    SnapshotConsensus[F, CurrencySnapshotArtifact, CurrencySnapshotContext, CurrencySnapshotEvent]
  ] = {
    def noopDecoder: Decoder[DataUpdate] = Decoder.failedWithMessage[DataUpdate]("not implemented")

    implicit def daDecoder: Decoder[DataUpdate] = maybeDataApplication.map(_.dataDecoder).getOrElse(noopDecoder)

    Consensus.make[F, CurrencySnapshotEvent, SnapshotOrdinal, CurrencySnapshotArtifact, CurrencySnapshotContext](
      CurrencySnapshotConsensusFunctions.make[F](
        stateChannelSnapshotService,
        collateral,
        maybeRewards,
        creator,
        validator
      ),
      gossip,
      selfId,
      keyPair,
      snapshotConfig.consensus,
      seedlist,
      clusterStorage,
      nodeStorage,
      client,
      session
    )
  }
}
