package org.shiwanetwork.currency.l0.modules

import cats.effect.kernel.Async
import cats.effect.std.{Random, Supervisor}
import cats.syntax.flatMap._
import cats.syntax.functor._

import org.shiwanetwork.currency.l0.node.IdentifierStorage
import org.shiwanetwork.currency.l0.snapshot.storages.LastBinaryHashStorage
import org.shiwanetwork.currency.schema.currency.{CurrencyIncrementalSnapshot, CurrencySnapshotInfo}
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.peer.L0Peer
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshotInfo}
import org.shiwanetwork.sdk.config.types.SnapshotConfig
import org.shiwanetwork.sdk.domain.cluster.storage.{ClusterStorage, L0ClusterStorage, SessionStorage}
import org.shiwanetwork.sdk.domain.collateral.LatestBalances
import org.shiwanetwork.sdk.domain.node.NodeStorage
import org.shiwanetwork.sdk.domain.snapshot.storage.{LastSnapshotStorage, SnapshotStorage}
import org.shiwanetwork.sdk.infrastructure.cluster.storage.L0ClusterStorage
import org.shiwanetwork.sdk.infrastructure.gossip.RumorStorage
import org.shiwanetwork.sdk.infrastructure.snapshot.storage.{LastSnapshotStorage, SnapshotLocalFileSystemStorage, SnapshotStorage}
import org.shiwanetwork.sdk.modules.SdkStorages

object Storages {

  def make[F[_]: Async: KryoSerializer: Supervisor: Random](
    sdkStorages: SdkStorages[F],
    snapshotConfig: SnapshotConfig,
    globalL0Peer: L0Peer
  ): F[Storages[F]] =
    for {
      snapshotLocalFileSystemStorage <- SnapshotLocalFileSystemStorage.make[F, CurrencyIncrementalSnapshot](
        snapshotConfig.incrementalPersistedSnapshotPath
      )
      snapshotStorage <- SnapshotStorage
        .make[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo](snapshotLocalFileSystemStorage, snapshotConfig.inMemoryCapacity)
      lastGlobalSnapshotStorage <- LastSnapshotStorage.make[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo]
      globalL0ClusterStorage <- L0ClusterStorage.make[F](globalL0Peer)
      lastBinaryHashStorage <- LastBinaryHashStorage.make[F]
      identifierStorage <- IdentifierStorage.make[F]
    } yield
      new Storages[F](
        globalL0Cluster = globalL0ClusterStorage,
        cluster = sdkStorages.cluster,
        node = sdkStorages.node,
        session = sdkStorages.session,
        rumor = sdkStorages.rumor,
        lastBinaryHash = lastBinaryHashStorage,
        snapshot = snapshotStorage,
        lastGlobalSnapshot = lastGlobalSnapshotStorage,
        incrementalSnapshotLocalFileSystemStorage = snapshotLocalFileSystemStorage,
        identifier = identifierStorage
      ) {}
}

sealed abstract class Storages[F[_]] private (
  val globalL0Cluster: L0ClusterStorage[F],
  val cluster: ClusterStorage[F],
  val node: NodeStorage[F],
  val session: SessionStorage[F],
  val rumor: RumorStorage[F],
  val lastBinaryHash: LastBinaryHashStorage[F],
  val snapshot: SnapshotStorage[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo] with LatestBalances[F],
  val lastGlobalSnapshot: LastSnapshotStorage[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo],
  val incrementalSnapshotLocalFileSystemStorage: SnapshotLocalFileSystemStorage[F, CurrencyIncrementalSnapshot],
  val identifier: IdentifierStorage[F]
)
