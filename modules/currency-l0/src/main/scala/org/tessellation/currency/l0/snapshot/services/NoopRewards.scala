package org.shiwanetwork.currency.l0.snapshot.services

import cats.effect.Async
import cats.syntax.applicative._

import scala.collection.immutable.{SortedMap, SortedSet}

import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.Balance
import org.shiwanetwork.schema.transaction.{RewardTransaction, Transaction}
import org.shiwanetwork.sdk.domain.rewards.Rewards
import org.shiwanetwork.sdk.infrastructure.consensus.trigger
import org.shiwanetwork.security.signature.Signed

object NoopRewards {
  def make[F[_]: Async]: Rewards[F, CurrencySnapshotStateProof, CurrencyIncrementalSnapshot] =
    (
      _: Signed[CurrencyIncrementalSnapshot],
      _: SortedMap[Address, Balance],
      _: SortedSet[Signed[Transaction]],
      _: trigger.ConsensusTrigger
    ) => SortedSet.empty[RewardTransaction].pure[F]
}
