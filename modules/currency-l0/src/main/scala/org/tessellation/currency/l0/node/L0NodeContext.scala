package org.shiwanetwork.currency.l0.node

import cats.data.OptionT
import cats.effect.Async
import cats.syntax.functor._

import org.shiwanetwork.currency.dataApplication.L0NodeContext
import org.shiwanetwork.currency.schema.currency.{CurrencyIncrementalSnapshot, CurrencySnapshotInfo}
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.sdk.domain.snapshot.storage.SnapshotStorage
import org.shiwanetwork.security.{Hashed, SecurityProvider}

object L0NodeContext {
  def make[F[_]: SecurityProvider: KryoSerializer: Async](
    snapshotStorage: SnapshotStorage[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo]
  ): L0NodeContext[F] = new L0NodeContext[F] {
    def securityProvider: SecurityProvider[F] = SecurityProvider[F]

    def getLastCurrencySnapshot: F[Option[Hashed[CurrencyIncrementalSnapshot]]] =
      OptionT(snapshotStorage.headSnapshot)
        .semiflatMap(_.toHashed)
        .value

    def getLastCurrencySnapshotCombined: F[Option[(Hashed[CurrencyIncrementalSnapshot], CurrencySnapshotInfo)]] =
      OptionT(snapshotStorage.head).semiflatMap {
        case (snapshot, info) => snapshot.toHashed.map((_, info))
      }.value

  }
}
