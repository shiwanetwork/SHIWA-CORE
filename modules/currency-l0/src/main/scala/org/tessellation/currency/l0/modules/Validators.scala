package org.shiwanetwork.currency.l0.modules

import cats.effect.Async

import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.sdk.domain.block.processing.BlockValidator
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.domain.transaction.{TransactionChainValidator, TransactionValidator}
import org.shiwanetwork.sdk.infrastructure.block.processing.BlockValidator
import org.shiwanetwork.sdk.infrastructure.gossip.RumorValidator
import org.shiwanetwork.security.SecurityProvider
import org.shiwanetwork.security.signature.SignedValidator

object Validators {

  def make[F[_]: Async: KryoSerializer: SecurityProvider](
    seedlist: Option[Set[SeedlistEntry]]
  ): Validators[F] = {
    val signedValidator = SignedValidator.make[F]
    val transactionChainValidator = TransactionChainValidator.make[F]
    val transactionValidator = TransactionValidator.make[F](signedValidator)
    val blockValidator =
      BlockValidator.make[F](signedValidator, transactionChainValidator, transactionValidator)
    val rumorValidator = RumorValidator.make[F](seedlist, signedValidator)

    new Validators[F](
      signedValidator,
      transactionChainValidator,
      transactionValidator,
      blockValidator,
      rumorValidator
    ) {}
  }
}

sealed abstract class Validators[F[_]] private (
  val signedValidator: SignedValidator[F],
  val transactionChainValidator: TransactionChainValidator[F],
  val transactionValidator: TransactionValidator[F],
  val blockValidator: BlockValidator[F],
  val rumorValidator: RumorValidator[F]
)
