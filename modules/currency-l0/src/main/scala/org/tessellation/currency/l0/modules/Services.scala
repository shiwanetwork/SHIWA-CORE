package org.shiwanetwork.currency.l0.modules

import java.security.KeyPair

import cats.effect.kernel.Async
import cats.effect.std.{Random, Supervisor}
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._

import org.shiwanetwork.currency.dataApplication.{BaseDataApplicationL0Service, L0NodeContext}
import org.shiwanetwork.currency.l0.config.types.AppConfig
import org.shiwanetwork.currency.l0.http.p2p.P2PClient
import org.shiwanetwork.currency.l0.node.L0NodeContext
import org.shiwanetwork.currency.l0.snapshot.services.StateChannelSnapshotService
import org.shiwanetwork.currency.l0.snapshot.{CurrencySnapshotConsensus, CurrencySnapshotEvent}
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.sdk.domain.cluster.services.{Cluster, Session}
import org.shiwanetwork.sdk.domain.collateral.Collateral
import org.shiwanetwork.sdk.domain.gossip.Gossip
import org.shiwanetwork.sdk.domain.healthcheck.LocalHealthcheck
import org.shiwanetwork.sdk.domain.rewards.Rewards
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.domain.snapshot.services.{AddressService, GlobalL0Service}
import org.shiwanetwork.sdk.infrastructure.Collateral
import org.shiwanetwork.sdk.infrastructure.metrics.Metrics
import org.shiwanetwork.sdk.infrastructure.snapshot._
import org.shiwanetwork.sdk.infrastructure.snapshot.services.AddressService
import org.shiwanetwork.sdk.modules.SdkServices
import org.shiwanetwork.security.SecurityProvider
import org.shiwanetwork.security.signature.SignedValidator

import org.http4s.client.Client

object Services {

  def make[F[_]: Async: Random: KryoSerializer: SecurityProvider: Metrics: Supervisor: L0NodeContext](
    p2PClient: P2PClient[F],
    sdkServices: SdkServices[F],
    storages: Storages[F],
    client: Client[F],
    session: Session[F],
    seedlist: Option[Set[SeedlistEntry]],
    selfId: PeerId,
    keyPair: KeyPair,
    cfg: AppConfig,
    maybeDataApplication: Option[BaseDataApplicationL0Service[F]],
    maybeRewards: Option[Rewards[F, CurrencySnapshotStateProof, CurrencyIncrementalSnapshot]],
    signedValidator: SignedValidator[F],
    globalSnapshotContextFns: GlobalSnapshotContextFunctions[F]
  ): F[Services[F]] =
    for {
      stateChannelSnapshotService <- StateChannelSnapshotService
        .make[F](
          keyPair,
          storages.lastBinaryHash,
          p2PClient.stateChannelSnapshot,
          storages.globalL0Cluster,
          storages.snapshot,
          storages.identifier
        )
        .pure[F]

      creator = CurrencySnapshotCreator.make[F](
        sdkServices.currencySnapshotAcceptanceManager,
        maybeDataApplication
          .map((L0NodeContext.make[F](storages.snapshot), _))
      )

      validator = CurrencySnapshotValidator.make[F](
        creator,
        signedValidator,
        maybeRewards,
        maybeDataApplication
      )

      consensus <- CurrencySnapshotConsensus
        .make[F](
          sdkServices.gossip,
          selfId,
          keyPair,
          seedlist,
          cfg.collateral.amount,
          storages.cluster,
          storages.node,
          maybeRewards,
          cfg.snapshot,
          client,
          session,
          stateChannelSnapshotService,
          maybeDataApplication,
          creator,
          validator
        )
      addressService = AddressService.make[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo](storages.snapshot)
      collateralService = Collateral.make[F](cfg.collateral, storages.snapshot)
      globalL0Service = GlobalL0Service
        .make[F](p2PClient.l0GlobalSnapshot, storages.globalL0Cluster, storages.lastGlobalSnapshot, None)
    } yield
      new Services[F](
        localHealthcheck = sdkServices.localHealthcheck,
        cluster = sdkServices.cluster,
        session = sdkServices.session,
        gossip = sdkServices.gossip,
        consensus = consensus,
        address = addressService,
        collateral = collateralService,
        stateChannelSnapshot = stateChannelSnapshotService,
        globalL0 = globalL0Service,
        snapshotContextFunctions = sdkServices.currencySnapshotContextFns,
        dataApplication = maybeDataApplication,
        globalSnapshotContextFunctions = globalSnapshotContextFns
      ) {}
}

sealed abstract class Services[F[_]] private (
  val localHealthcheck: LocalHealthcheck[F],
  val cluster: Cluster[F],
  val session: Session[F],
  val gossip: Gossip[F],
  val consensus: SnapshotConsensus[
    F,
    CurrencyIncrementalSnapshot,
    CurrencySnapshotContext,
    CurrencySnapshotEvent
  ],
  val address: AddressService[F, CurrencyIncrementalSnapshot],
  val collateral: Collateral[F],
  val stateChannelSnapshot: StateChannelSnapshotService[F],
  val globalL0: GlobalL0Service[F],
  val snapshotContextFunctions: CurrencySnapshotContextFunctions[F],
  val dataApplication: Option[BaseDataApplicationL0Service[F]],
  val globalSnapshotContextFunctions: GlobalSnapshotContextFunctions[F]
)
