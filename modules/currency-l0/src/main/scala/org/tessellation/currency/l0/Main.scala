package org.shiwanetwork.currency.l0

import java.util.UUID

import cats.effect.IO

import org.shiwanetwork.BuildInfo
import org.shiwanetwork.currency.dataApplication.BaseDataApplicationL0Service
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.schema.cluster.ClusterId
import org.shiwanetwork.sdk.domain.rewards.Rewards
import org.shiwanetwork.security.SecurityProvider

object Main
    extends CurrencyL0App(
      "Currency-l0",
      "Currency L0 node",
      ClusterId(UUID.fromString("517c3a05-9219-471b-a54c-21b7d72f4ae5")),
      version = BuildInfo.version
    ) {

  def dataApplication: Option[BaseDataApplicationL0Service[IO]] = None

  def rewards(implicit sp: SecurityProvider[IO]): Option[Rewards[
    IO,
    CurrencySnapshotStateProof,
    CurrencyIncrementalSnapshot
  ]] = None
}
