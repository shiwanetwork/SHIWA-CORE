package org.shiwanetwork.infrastructure.trust

import cats.effect.Async
import cats.syntax.flatMap._
import cats.syntax.show._

import org.shiwanetwork.schema.gossip.PeerRumor
import org.shiwanetwork.schema.trust.{PublicTrust, SnapshotOrdinalPublicTrust}
import org.shiwanetwork.sdk.domain.trust.storage.TrustStorage
import org.shiwanetwork.sdk.infrastructure.gossip.{IgnoreSelfOrigin, RumorHandler, rumorLoggerName}

import org.typelevel.log4cats.slf4j.Slf4jLogger

object handler {

  def trustHandler[F[_]: Async](
    trustStorage: TrustStorage[F]
  ): RumorHandler[F] = {
    val logger = Slf4jLogger.getLoggerFromName[F](rumorLoggerName)

    RumorHandler.fromPeerRumorConsumer[F, PublicTrust](IgnoreSelfOrigin) {
      case PeerRumor(origin, _, trust) =>
        logger.info(s"Received trust=${trust} from id=${origin.show}") >> {
          trustStorage.updatePeerPublicTrustInfo(origin, trust)
        }
    }
  }

  def ordinalTrustHandler[F[_]: Async](
    trustStorage: TrustStorage[F]
  ): RumorHandler[F] = {
    val logger = Slf4jLogger.getLoggerFromName[F](rumorLoggerName)

    RumorHandler.fromPeerRumorConsumer[F, SnapshotOrdinalPublicTrust](IgnoreSelfOrigin) {
      case PeerRumor(origin, _, trust) =>
        logger.debug(s"Received trust=${trust} from id=${origin.show}") >>
          trustStorage.updateNext(origin, trust)
    }
  }
}
