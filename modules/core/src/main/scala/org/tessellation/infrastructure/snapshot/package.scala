package org.shiwanetwork.infrastructure

import org.shiwanetwork.schema._
import org.shiwanetwork.sdk.infrastructure.snapshot.SnapshotConsensus
import org.shiwanetwork.security.signature.Signed
import org.shiwanetwork.statechannel.StateChannelOutput

package object snapshot {

  type DAGEvent = Signed[Block]

  type StateChannelEvent = StateChannelOutput

  type GlobalSnapshotEvent = Either[StateChannelEvent, DAGEvent]

  type GlobalSnapshotKey = SnapshotOrdinal

  type GlobalSnapshotArtifact = GlobalIncrementalSnapshot

  type GlobalSnapshotContext = GlobalSnapshotInfo

  type GlobalSnapshotConsensus[F[_]] =
    SnapshotConsensus[F, GlobalSnapshotArtifact, GlobalSnapshotContext, GlobalSnapshotEvent]

}
