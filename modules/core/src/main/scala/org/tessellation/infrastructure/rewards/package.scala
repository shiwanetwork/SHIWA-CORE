package org.shiwanetwork.infrastructure

import cats.data.StateT

import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.Amount

package object rewards {

  type DistributionState[F[_]] = StateT[F, Amount, List[(Address, Amount)]]

}
