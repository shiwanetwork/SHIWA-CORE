package org.shiwanetwork.infrastructure.snapshot

import java.security.KeyPair

import cats.data.NonEmptySet
import cats.effect.kernel.Async
import cats.effect.std.{Random, Supervisor}
import cats.syntax.flatMap._
import cats.syntax.functor._

import org.shiwanetwork.cli.AppEnvironment
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.balance.Amount
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshotStateProof}
import org.shiwanetwork.sdk.config.types.SnapshotConfig
import org.shiwanetwork.sdk.domain.cluster.services.Session
import org.shiwanetwork.sdk.domain.cluster.storage.ClusterStorage
import org.shiwanetwork.sdk.domain.gossip.Gossip
import org.shiwanetwork.sdk.domain.node.NodeStorage
import org.shiwanetwork.sdk.domain.rewards.Rewards
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.domain.snapshot.storage.SnapshotStorage
import org.shiwanetwork.sdk.infrastructure.block.processing.BlockAcceptanceManager
import org.shiwanetwork.sdk.infrastructure.consensus.Consensus
import org.shiwanetwork.sdk.infrastructure.metrics.Metrics
import org.shiwanetwork.sdk.infrastructure.snapshot.{
  GlobalSnapshotAcceptanceManager,
  GlobalSnapshotStateChannelAcceptanceManager,
  GlobalSnapshotStateChannelEventsProcessor
}
import org.shiwanetwork.sdk.modules.{SdkServices, SdkValidators}
import org.shiwanetwork.security.SecurityProvider

import eu.timepit.refined.types.numeric.NonNegLong
import io.circe.disjunctionCodecs._
import org.http4s.client.Client

object GlobalSnapshotConsensus {

  def make[F[_]: Async: Random: KryoSerializer: SecurityProvider: Metrics: Supervisor](
    gossip: Gossip[F],
    selfId: PeerId,
    keyPair: KeyPair,
    seedlist: Option[Set[SeedlistEntry]],
    collateral: Amount,
    clusterStorage: ClusterStorage[F],
    nodeStorage: NodeStorage[F],
    globalSnapshotStorage: SnapshotStorage[F, GlobalSnapshotArtifact, GlobalSnapshotContext],
    validators: SdkValidators[F],
    sdkServices: SdkServices[F],
    snapshotConfig: SnapshotConfig,
    environment: AppEnvironment,
    stateChannelPullDelay: NonNegLong,
    stateChannelPurgeDelay: NonNegLong,
    stateChannelAllowanceLists: Option[Map[Address, NonEmptySet[PeerId]]],
    client: Client[F],
    session: Session[F],
    rewards: Rewards[F, GlobalSnapshotStateProof, GlobalIncrementalSnapshot]
  ): F[Consensus[F, GlobalSnapshotEvent, GlobalSnapshotKey, GlobalSnapshotArtifact, GlobalSnapshotContext]] =
    for {
      globalSnapshotStateChannelManager <- GlobalSnapshotStateChannelAcceptanceManager
        .make[F](stateChannelAllowanceLists, pullDelay = stateChannelPullDelay, purgeDelay = stateChannelPurgeDelay)
      snapshotAcceptanceManager = GlobalSnapshotAcceptanceManager.make(
        BlockAcceptanceManager.make[F](validators.blockValidator),
        GlobalSnapshotStateChannelEventsProcessor
          .make[F](validators.stateChannelValidator, globalSnapshotStateChannelManager, sdkServices.currencySnapshotContextFns),
        collateral
      )
      consensus <- Consensus.make[F, GlobalSnapshotEvent, GlobalSnapshotKey, GlobalSnapshotArtifact, GlobalSnapshotContext](
        GlobalSnapshotConsensusFunctions.make[F](
          globalSnapshotStorage,
          snapshotAcceptanceManager,
          collateral,
          rewards,
          environment
        ),
        gossip,
        selfId,
        keyPair,
        snapshotConfig.consensus,
        seedlist,
        clusterStorage,
        nodeStorage,
        client,
        session
      )
    } yield consensus
}
