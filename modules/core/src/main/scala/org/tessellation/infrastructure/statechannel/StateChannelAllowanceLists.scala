package org.shiwanetwork.infrastructure.statechannel

import cats.data.NonEmptySet
import cats.syntax.option._

import org.shiwanetwork.cli.AppEnvironment
import org.shiwanetwork.cli.AppEnvironment._
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.peer.PeerId

object StateChannelAllowanceLists {

  def get(env: AppEnvironment): Option[Map[Address, NonEmptySet[PeerId]]] =
    env match {
      case Dev | Testnet | Integrationnet => none
      case Mainnet                        => Map.empty[Address, NonEmptySet[PeerId]].some
    }

}
