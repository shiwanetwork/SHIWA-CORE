package org.shiwanetwork.domain.cell

import org.shiwanetwork.kernel.Ω
import org.shiwanetwork.schema.Block
import org.shiwanetwork.security.signature.Signed
import org.shiwanetwork.statechannel.StateChannelOutput

sealed trait AlgebraCommand extends Ω

object AlgebraCommand {
  case class EnqueueStateChannelSnapshot(snapshot: StateChannelOutput) extends AlgebraCommand
  case class EnqueueDAGL1Data(data: Signed[Block]) extends AlgebraCommand
  case object NoAction extends AlgebraCommand
}
