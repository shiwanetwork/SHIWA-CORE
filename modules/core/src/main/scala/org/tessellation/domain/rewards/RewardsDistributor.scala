package org.shiwanetwork.domain.rewards

import cats.data.NonEmptySet

import org.shiwanetwork.infrastructure.rewards.DistributionState
import org.shiwanetwork.schema.ID.Id
import org.shiwanetwork.schema.epoch.EpochProgress

trait RewardsDistributor[F[_]] {

  def distribute(epochProgress: EpochProgress, facilitators: NonEmptySet[Id]): DistributionState[F]

}
