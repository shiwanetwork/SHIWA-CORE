package org.shiwanetwork.http.p2p

import cats.effect.Async
import cats.syntax.option._

import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.sdk.domain.cluster.services.Session
import org.shiwanetwork.sdk.http.p2p.SdkP2PClient
import org.shiwanetwork.sdk.http.p2p.clients._
import org.shiwanetwork.sdk.infrastructure.gossip.p2p.GossipClient
import org.shiwanetwork.security.SecurityProvider

import org.http4s.client.Client

object P2PClient {

  def make[F[_]: Async: SecurityProvider: KryoSerializer](
    sdkP2PClient: SdkP2PClient[F],
    client: Client[F],
    session: Session[F]
  ): P2PClient[F] =
    new P2PClient[F](
      sdkP2PClient.sign,
      sdkP2PClient.cluster,
      sdkP2PClient.gossip,
      sdkP2PClient.node,
      L0GlobalSnapshotClient.make(client, session.some)
    ) {}
}

sealed abstract class P2PClient[F[_]] private (
  val sign: SignClient[F],
  val cluster: ClusterClient[F],
  val gossip: GossipClient[F],
  val node: NodeClient[F],
  val globalSnapshot: L0GlobalSnapshotClient[F]
)
