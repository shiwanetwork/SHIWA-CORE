package org.shiwanetwork.http.routes

import cats.effect.Async
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import cats.syntax.functor._

import org.shiwanetwork.domain.cluster.programs.TrustPush
import org.shiwanetwork.http.routes.internal.{CliRoutes, InternalUrlPrefix, P2PRoutes}
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.trust.{PeerObservationAdjustmentUpdateBatch, TrustScores}
import org.shiwanetwork.sdk.domain.trust.storage.TrustStorage
import org.shiwanetwork.sdk.ext.http4s.refined.RefinedRequestDecoder

import eu.timepit.refined.auto._
import org.http4s._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.dsl.Http4sDsl

final case class TrustRoutes[F[_]: Async: KryoSerializer](
  trustStorage: TrustStorage[F],
  trustPush: TrustPush[F]
) extends Http4sDsl[F]
    with P2PRoutes[F]
    with CliRoutes[F] {
  protected[routes] val prefixPath: InternalUrlPrefix = "/trust"

  protected val p2p: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root =>
      trustStorage.getPublicTrust.flatMap { publicTrust =>
        Ok(publicTrust)
      }
  }

  protected val cli: HttpRoutes[F] = HttpRoutes.of[F] {
    case req @ POST -> Root =>
      req.decodeR[PeerObservationAdjustmentUpdateBatch] { trustUpdates =>
        trustStorage
          .updateTrust(trustUpdates)
          .flatMap(_ => trustPush.publishUpdated())
          .flatMap(_ => Ok())
          .recoverWith {
            case _ =>
              Conflict(s"Internal trust update failure")
          }
      }

    case GET -> Root / "current" =>
      trustStorage.getTrust
        .map(_.trust.view.mapValues(_.predictedTrust))
        .map(_.collect { case (k, Some(v)) => (k, v) }.toMap)
        .flatMap(trust => Ok(TrustScores(trust)))

    case GET -> Root / "previous" =>
      trustStorage.getCurrentOrdinalTrust.flatMap(Ok(_))
  }
}
