package org.shiwanetwork.modules

import java.security.KeyPair

import cats.data.NonEmptySet
import cats.effect.kernel.Async
import cats.effect.std.{Random, Supervisor}
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._

import org.shiwanetwork.config.types.AppConfig
import org.shiwanetwork.domain.cell.L0Cell
import org.shiwanetwork.domain.statechannel.StateChannelService
import org.shiwanetwork.infrastructure.rewards._
import org.shiwanetwork.infrastructure.snapshot._
import org.shiwanetwork.infrastructure.trust.TrustStorageUpdater
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.address.Address
import org.shiwanetwork.schema.peer.PeerId
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshotInfo, GlobalSnapshotStateProof}
import org.shiwanetwork.sdk.domain.cluster.services.{Cluster, Session}
import org.shiwanetwork.sdk.domain.collateral.Collateral
import org.shiwanetwork.sdk.domain.gossip.Gossip
import org.shiwanetwork.sdk.domain.healthcheck.LocalHealthcheck
import org.shiwanetwork.sdk.domain.rewards.Rewards
import org.shiwanetwork.sdk.domain.seedlist.SeedlistEntry
import org.shiwanetwork.sdk.domain.snapshot.services.AddressService
import org.shiwanetwork.sdk.infrastructure.Collateral
import org.shiwanetwork.sdk.infrastructure.consensus.Consensus
import org.shiwanetwork.sdk.infrastructure.metrics.Metrics
import org.shiwanetwork.sdk.infrastructure.snapshot.services.AddressService
import org.shiwanetwork.sdk.modules.{SdkServices, SdkValidators}
import org.shiwanetwork.security.SecurityProvider

import eu.timepit.refined.auto._
import org.http4s.client.Client

object Services {

  def make[F[_]: Async: Random: KryoSerializer: SecurityProvider: Metrics: Supervisor](
    sdkServices: SdkServices[F],
    queues: Queues[F],
    storages: Storages[F],
    validators: SdkValidators[F],
    client: Client[F],
    session: Session[F],
    seedlist: Option[Set[SeedlistEntry]],
    stateChannelAllowanceLists: Option[Map[Address, NonEmptySet[PeerId]]],
    selfId: PeerId,
    keyPair: KeyPair,
    cfg: AppConfig
  ): F[Services[F]] =
    for {
      rewards <- Rewards
        .make[F](
          cfg.rewards,
          ProgramsDistributor.make,
          FacilitatorDistributor.make
        )
        .pure[F]

      consensus <- GlobalSnapshotConsensus
        .make[F](
          sdkServices.gossip,
          selfId,
          keyPair,
          seedlist,
          cfg.collateral.amount,
          storages.cluster,
          storages.node,
          storages.globalSnapshot,
          validators,
          sdkServices,
          cfg.snapshot,
          cfg.environment,
          stateChannelPullDelay = cfg.stateChannelPullDelay,
          stateChannelPurgeDelay = cfg.stateChannelPurgeDelay,
          stateChannelAllowanceLists,
          client,
          session,
          rewards
        )
      addressService = AddressService.make[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo](storages.globalSnapshot)
      collateralService = Collateral.make[F](cfg.collateral, storages.globalSnapshot)
      stateChannelService = StateChannelService
        .make[F](L0Cell.mkL0Cell(queues.l1Output, queues.stateChannelOutput), validators.stateChannelValidator)
      getOrdinal = storages.globalSnapshot.headSnapshot.map(_.map(_.ordinal))
      trustUpdaterService = TrustStorageUpdater.make(getOrdinal, sdkServices.gossip, storages.trust)
    } yield
      new Services[F](
        localHealthcheck = sdkServices.localHealthcheck,
        cluster = sdkServices.cluster,
        session = sdkServices.session,
        gossip = sdkServices.gossip,
        consensus = consensus,
        address = addressService,
        collateral = collateralService,
        rewards = rewards,
        stateChannel = stateChannelService,
        trustStorageUpdater = trustUpdaterService
      ) {}
}

sealed abstract class Services[F[_]] private (
  val localHealthcheck: LocalHealthcheck[F],
  val cluster: Cluster[F],
  val session: Session[F],
  val gossip: Gossip[F],
  val consensus: Consensus[F, GlobalSnapshotEvent, GlobalSnapshotKey, GlobalSnapshotArtifact, GlobalSnapshotContext],
  val address: AddressService[F, GlobalIncrementalSnapshot],
  val collateral: Collateral[F],
  val rewards: Rewards[F, GlobalSnapshotStateProof, GlobalIncrementalSnapshot],
  val stateChannel: StateChannelService[F],
  val trustStorageUpdater: TrustStorageUpdater[F]
)
