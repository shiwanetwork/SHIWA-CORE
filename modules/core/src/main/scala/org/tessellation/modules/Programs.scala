package org.shiwanetwork.modules

import java.security.KeyPair

import cats.effect.Async
import cats.effect.std.Random

import org.shiwanetwork.config.types.AppConfig
import org.shiwanetwork.domain.cluster.programs.TrustPush
import org.shiwanetwork.domain.snapshot.programs.Download
import org.shiwanetwork.http.p2p.P2PClient
import org.shiwanetwork.infrastructure.snapshot.programs.RollbackLoader
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.SnapshotOrdinal
import org.shiwanetwork.sdk.domain.cluster.programs.{Joining, PeerDiscovery}
import org.shiwanetwork.sdk.domain.snapshot.PeerSelect
import org.shiwanetwork.sdk.domain.snapshot.programs.Download
import org.shiwanetwork.sdk.infrastructure.snapshot.{GlobalSnapshotContextFunctions, MajorityPeerSelect}
import org.shiwanetwork.sdk.modules.SdkPrograms
import org.shiwanetwork.security.SecurityProvider

object Programs {

  def make[F[_]: Async: KryoSerializer: SecurityProvider: Random](
    sdkPrograms: SdkPrograms[F],
    storages: Storages[F],
    services: Services[F],
    keyPair: KeyPair,
    config: AppConfig,
    lastFullGlobalSnapshotOrdinal: SnapshotOrdinal,
    p2pClient: P2PClient[F],
    globalSnapshotContextFns: GlobalSnapshotContextFunctions[F]
  ): Programs[F] = {
    val trustPush = TrustPush.make(storages.trust, services.gossip)
    val peerSelect: PeerSelect[F] = MajorityPeerSelect.make(storages.cluster, p2pClient.globalSnapshot)
    val download: Download[F] = Download
      .make[F](
        storages.snapshotDownload,
        p2pClient,
        storages.cluster,
        lastFullGlobalSnapshotOrdinal,
        globalSnapshotContextFns: GlobalSnapshotContextFunctions[F],
        storages.node,
        services.consensus,
        peerSelect
      )
    val rollbackLoader = RollbackLoader.make(
      keyPair,
      config.snapshot,
      storages.incrementalGlobalSnapshotLocalFileSystemStorage,
      globalSnapshotContextFns
    )

    new Programs[F](sdkPrograms.peerDiscovery, sdkPrograms.joining, trustPush, download, rollbackLoader) {}
  }
}

sealed abstract class Programs[F[_]] private (
  val peerDiscovery: PeerDiscovery[F],
  val joining: Joining[F],
  val trustPush: TrustPush[F],
  val download: Download[F],
  val rollbackLoader: RollbackLoader[F]
)
