package org.shiwanetwork

import cats.effect._
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.semigroupk._

import org.shiwanetwork.cli.method._
import org.shiwanetwork.ext.cats.effect._
import org.shiwanetwork.ext.kryo._
import org.shiwanetwork.http.p2p.P2PClient
import org.shiwanetwork.infrastructure.trust.handler.{ordinalTrustHandler, trustHandler}
import org.shiwanetwork.modules._
import org.shiwanetwork.schema.cluster.ClusterId
import org.shiwanetwork.schema.node.NodeState
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshot}
import org.shiwanetwork.sdk.app.{SDK, shiwanetworkIOApp}
import org.shiwanetwork.sdk.domain.collateral.OwnCollateralNotSatisfied
import org.shiwanetwork.sdk.infrastructure.genesis.{Loader => GenesisLoader}
import org.shiwanetwork.sdk.infrastructure.gossip.{GossipDaemon, RumorHandlers}
import org.shiwanetwork.sdk.infrastructure.snapshot.storage.SnapshotLocalFileSystemStorage
import org.shiwanetwork.sdk.resources.MkHttpServer
import org.shiwanetwork.sdk.resources.MkHttpServer.ServerName
import org.shiwanetwork.security.signature.Signed

import com.monovore.decline.Opts
import eu.timepit.refined.auto._

object Main
    extends shiwanetworkIOApp[Run](
      name = "dag-l0",
      header = "shiwanetwork Node",
      version = BuildInfo.version,
      clusterId = ClusterId("6d7f1d6a-213a-4148-9d45-d7200f555ecf")
    ) {

  val opts: Opts[Run] = cli.method.opts

  type KryoRegistrationIdRange = CoreKryoRegistrationIdRange

  val kryoRegistrar: Map[Class[_], KryoRegistrationId[KryoRegistrationIdRange]] =
    coreKryoRegistrar

  def run(method: Run, sdk: SDK[IO]): Resource[IO, Unit] = {
    import sdk._

    val cfg = method.appConfig

    for {
      queues <- Queues.make[IO](sdkQueues).asResource
      p2pClient = P2PClient.make[IO](sdkP2PClient, sdkResources.client, sdkServices.session)
      storages <- Storages
        .make[IO](
          sdkStorages,
          method.sdkConfig,
          sdk.seedlist,
          cfg.snapshot,
          trustRatings
        )
        .asResource
      services <- Services
        .make[IO](
          sdkServices,
          queues,
          storages,
          sdk.sdkValidators,
          sdkResources.client,
          sdkServices.session,
          sdk.seedlist,
          method.stateChannelAllowanceLists,
          sdk.nodeId,
          keyPair,
          cfg
        )
        .asResource
      programs = Programs.make[IO](
        sdkPrograms,
        storages,
        services,
        keyPair,
        cfg,
        method.lastFullGlobalSnapshotOrdinal,
        p2pClient,
        sdkServices.globalSnapshotContextFns
      )
      healthChecks <- HealthChecks
        .make[IO](
          storages,
          services,
          programs,
          p2pClient,
          sdkResources.client,
          sdkServices.session,
          cfg.healthCheck,
          sdk.nodeId
        )
        .asResource

      rumorHandler = RumorHandlers.make[IO](storages.cluster, healthChecks.ping, services.localHealthcheck).handlers <+>
        trustHandler(storages.trust) <+> ordinalTrustHandler(storages.trust) <+> services.consensus.handler

      _ <- Daemons
        .start(storages, services, programs, queues, healthChecks, nodeId, cfg)
        .asResource

      api = HttpApi
        .make[IO](
          storages,
          queues,
          services,
          programs,
          healthChecks,
          keyPair.getPrivate,
          cfg.environment,
          sdk.nodeId,
          BuildInfo.version,
          cfg.http
        )
      _ <- MkHttpServer[IO].newEmber(ServerName("public"), cfg.http.publicHttp, api.publicApp)
      _ <- MkHttpServer[IO].newEmber(ServerName("p2p"), cfg.http.p2pHttp, api.p2pApp)
      _ <- MkHttpServer[IO].newEmber(ServerName("cli"), cfg.http.cliHttp, api.cliApp)

      gossipDaemon = GossipDaemon.make[IO](
        storages.rumor,
        queues.rumor,
        storages.cluster,
        p2pClient.gossip,
        rumorHandler,
        sdk.sdkValidators.rumorValidator,
        services.localHealthcheck,
        nodeId,
        generation,
        cfg.gossip.daemon,
        services.collateral
      )

      _ <- (method match {
        case _: RunValidator =>
          gossipDaemon.startAsRegularValidator >>
            storages.node.tryModifyState(NodeState.Initial, NodeState.ReadyToJoin)
        case m: RunRollback =>
          storages.node.tryModifyState(
            NodeState.Initial,
            NodeState.RollbackInProgress,
            NodeState.RollbackDone
          ) {
            programs.rollbackLoader.load(m.rollbackHash).flatMap {
              case (snapshotInfo, snapshot) =>
                storages.globalSnapshot
                  .prepend(snapshot, snapshotInfo) >>
                  services.consensus.manager.startFacilitatingAfterRollback(snapshot.ordinal, snapshot, snapshotInfo)
            }
          } >>
            services.collateral
              .hasCollateral(sdk.nodeId)
              .flatMap(OwnCollateralNotSatisfied.raiseError[IO, Unit].unlessA) >>
            gossipDaemon.startAsInitialValidator >>
            services.cluster.createSession >>
            services.session.createSession >>
            storages.node.setNodeState(NodeState.Ready)
        case m: RunGenesis =>
          storages.node.tryModifyState(
            NodeState.Initial,
            NodeState.LoadingGenesis,
            NodeState.GenesisReady
          ) {
            GenesisLoader.make[IO].load(m.genesisPath).flatMap { accounts =>
              val genesis = GlobalSnapshot.mkGenesis(
                accounts.map(a => (a.address, a.balance)).toMap,
                m.startingEpochProgress
              )

              Signed.forAsyncKryo[IO, GlobalSnapshot](genesis, keyPair).flatMap(_.toHashed[IO]).flatMap { hashedGenesis =>
                SnapshotLocalFileSystemStorage.make[IO, GlobalSnapshot](cfg.snapshot.snapshotPath).flatMap {
                  fullGlobalSnapshotLocalFileSystemStorage =>
                    fullGlobalSnapshotLocalFileSystemStorage.write(hashedGenesis.signed) >>
                      GlobalSnapshot.mkFirstIncrementalSnapshot[IO](hashedGenesis).flatMap { firstIncrementalSnapshot =>
                        Signed.forAsyncKryo[IO, GlobalIncrementalSnapshot](firstIncrementalSnapshot, keyPair).flatMap {
                          signedFirstIncrementalSnapshot =>
                            storages.globalSnapshot.prepend(signedFirstIncrementalSnapshot, hashedGenesis.info) >>
                              services.collateral
                                .hasCollateral(sdk.nodeId)
                                .flatMap(OwnCollateralNotSatisfied.raiseError[IO, Unit].unlessA) >>
                              services.consensus.manager
                                .startFacilitatingAfterRollback(
                                  signedFirstIncrementalSnapshot.ordinal,
                                  signedFirstIncrementalSnapshot,
                                  hashedGenesis.info
                                )

                        }
                      }
                }
              }
            }
          } >>
            gossipDaemon.startAsInitialValidator >>
            services.cluster.createSession >>
            services.session.createSession >>
            storages.node.setNodeState(NodeState.Ready)
      }).asResource
    } yield ()
  }
}
