package org.shiwanetwork.cli

import org.shiwanetwork.cli.AppEnvironment
import org.shiwanetwork.cli.AppEnvironment.{Integrationnet, Mainnet, Testnet}
import org.shiwanetwork.ext.decline.decline._
import org.shiwanetwork.schema.SnapshotOrdinal

import com.monovore.decline.Opts
import com.monovore.decline.refined._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.numeric.NonNegLong

object incremental {
  val lastFullGlobalSnapshot: Map[AppEnvironment, SnapshotOrdinal] = Map(
    Mainnet -> SnapshotOrdinal(0L), // TODO: set before mainnet release
    Testnet -> SnapshotOrdinal(736766L),
    Integrationnet -> SnapshotOrdinal(0L)
  )

  val lastFullGlobalSnapshotOrdinalOpts: Opts[Option[SnapshotOrdinal]] = Opts
    .argument[NonNegLong]("lastFullGlobalSnapshotOrdinal")
    .map(SnapshotOrdinal(_))
    .orNone
}
