package org.shiwanetwork.currency.l1.node

import org.shiwanetwork.currency.dataApplication.L1NodeContext
import org.shiwanetwork.currency.schema.currency.{CurrencyIncrementalSnapshot, CurrencySnapshotInfo}
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshotInfo}
import org.shiwanetwork.sdk.domain.snapshot.storage.LastSnapshotStorage
import org.shiwanetwork.security.{Hashed, SecurityProvider}

object L1NodeContext {
  def make[F[_]: SecurityProvider](
    lastGlobalSnapshotStorage: LastSnapshotStorage[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo],
    lastCurrencySnapshotStorage: LastSnapshotStorage[F, CurrencyIncrementalSnapshot, CurrencySnapshotInfo]
  ): L1NodeContext[F] =
    new L1NodeContext[F] {
      def getLastGlobalSnapshot: F[Option[Hashed[GlobalIncrementalSnapshot]]] = lastGlobalSnapshotStorage.get

      def getLastCurrencySnapshot: F[Option[Hashed[CurrencyIncrementalSnapshot]]] = lastCurrencySnapshotStorage.get

      def getLastCurrencySnapshotCombined: F[Option[(Hashed[CurrencyIncrementalSnapshot], CurrencySnapshotInfo)]] =
        lastCurrencySnapshotStorage.getCombined

      def securityProvider: SecurityProvider[F] = SecurityProvider[F]
    }
}
