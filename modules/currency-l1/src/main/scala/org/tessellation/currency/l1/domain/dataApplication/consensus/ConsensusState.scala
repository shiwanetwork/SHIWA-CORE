package org.shiwanetwork.currency.l1.domain.dataApplication.consensus

import org.shiwanetwork.schema.round.RoundId

case class ConsensusState(
  ownConsensus: Option[RoundData],
  peerConsensuses: Map[RoundId, RoundData]
)

object ConsensusState {
  def Empty = ConsensusState(None, Map.empty)
}
