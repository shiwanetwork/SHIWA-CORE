package org.shiwanetwork.currency.l1.modules

import cats.effect.kernel.Async
import cats.effect.std.Random

import org.shiwanetwork.currency.dataApplication.BaseDataApplicationL1Service
import org.shiwanetwork.currency.l1.http.p2p.P2PClient
import org.shiwanetwork.currency.schema.currency._
import org.shiwanetwork.dag.l1.config.types.AppConfig
import org.shiwanetwork.dag.l1.domain.block.BlockService
import org.shiwanetwork.dag.l1.domain.transaction.TransactionService
import org.shiwanetwork.dag.l1.modules.{Services => BaseServices, Validators}
import org.shiwanetwork.kryo.KryoSerializer
import org.shiwanetwork.schema.snapshot.{Snapshot, SnapshotInfo, StateProof}
import org.shiwanetwork.schema.{GlobalIncrementalSnapshot, GlobalSnapshotInfo}
import org.shiwanetwork.sdk.domain.cluster.storage.L0ClusterStorage
import org.shiwanetwork.sdk.domain.snapshot.services.GlobalL0Service
import org.shiwanetwork.sdk.domain.snapshot.storage.LastSnapshotStorage
import org.shiwanetwork.sdk.infrastructure.Collateral
import org.shiwanetwork.sdk.infrastructure.block.processing.BlockAcceptanceManager
import org.shiwanetwork.sdk.modules.SdkServices
import org.shiwanetwork.security.SecurityProvider

object Services {
  def make[F[_]: Async: Random: KryoSerializer: SecurityProvider](
    storages: Storages[
      F,
      CurrencySnapshotStateProof,
      CurrencyIncrementalSnapshot,
      CurrencySnapshotInfo
    ],
    lastGlobalSnapshotStorage: LastSnapshotStorage[F, GlobalIncrementalSnapshot, GlobalSnapshotInfo],
    globalL0Cluster: L0ClusterStorage[F],
    validators: Validators[F],
    sdkServices: SdkServices[F],
    p2PClient: P2PClient[F],
    cfg: AppConfig,
    maybeDataApplication: Option[BaseDataApplicationL1Service[F]]
  ): Services[F, CurrencySnapshotStateProof, CurrencyIncrementalSnapshot, CurrencySnapshotInfo] =
    new Services[F, CurrencySnapshotStateProof, CurrencyIncrementalSnapshot, CurrencySnapshotInfo] {

      val localHealthcheck = sdkServices.localHealthcheck
      val block = BlockService.make[F](
        BlockAcceptanceManager.make[F](validators.block),
        storages.address,
        storages.block,
        storages.transaction,
        cfg.collateral.amount
      )
      val cluster = sdkServices.cluster
      val gossip = sdkServices.gossip
      val globalL0 =
        GlobalL0Service.make[F](p2PClient.l0GlobalSnapshot, globalL0Cluster, lastGlobalSnapshotStorage, None)
      val session = sdkServices.session
      val transaction = TransactionService.make[F](storages.transaction, validators.transactionContextual)
      val collateral = Collateral.make[F](cfg.collateral, storages.lastSnapshot)
      val dataApplication = maybeDataApplication
    }
}

sealed abstract class Services[F[_], P <: StateProof, S <: Snapshot, SI <: SnapshotInfo[P]] extends BaseServices[F, P, S, SI] {
  val dataApplication: Option[BaseDataApplicationL1Service[F]]
}
