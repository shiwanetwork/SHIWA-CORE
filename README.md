ShiwaNetwork
===========

![build](https://img.shields.io/label=build)
![version](https://img.shields.io/)

### Развертывание L0 и L1 в Kubernetes
=====================================

### Предустановки

1. [sbt](https://www.scala-sbt.org/)
2. [Docker Desktop](https://www.docker.com/get-started/) with [Kubernetes](https://docs.docker.com/desktop/kubernetes/) enabled
3. [Skaffold CLI](https://skaffold.dev/docs/install/#standalone-binary)

### Запуск кластера

```
skaffold dev --trigger=manual
```


Это запустит оба кластера L0 и L1 в Kubernetes при условии текущей контекстной сессии.

Инициализаторы для L0 и L1 имеют свои публичные порты. Маппированы к локальным портам 9000 и 9010 соответственно.


```
curl localhost:9000/cluster/info
curl localhost:9010/cluster/info
```


Они будут возвращать список инициализаторов L0 и L1. По умолчанию, оба кластера L0 и L1 начинаются с трех инициализаторов - 1 начальный и 2 регулярных.

--------------------


### Увеличение кластера
```
kubectl scale deployment/l0-validator-deployment --replicas=9
```

Это расширит кластер L0 до 10 инициализаторов, 1 начальный и 9 регулярных.
